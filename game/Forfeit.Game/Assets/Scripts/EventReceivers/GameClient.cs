﻿using Forfeit.Models;
using System;
using System.Threading.Tasks;

namespace Forfeit.EventReceivers
{
    public interface IGameClient : IEventClient
    {
        Action<OfferModel> OnAnswer { get; set; }
        Action<string> OnPlayerAdded { get; set; }
        Action<string> OnPlayerRemoved { get; set; }
        Action<string> OnPlayerReady { get; set; }
        Action<CandidateModel> OnCandidate { get; set; }
        Action<string> OnConnectionIdBroadcast { get; set; }

        Task AddToGroup(string code);
        Task Candidate(CandidateModel candidate);
        Task Offer(OfferModel offer);
        Task SetInput(SetInputModel model);
    }

    public class GameClient : IGameClient
    {
        public const string BUTTON_DOWN = "ButtonDown";
        public const string BUTTON_UP = "ButtonUp";
        public const string MOVE = "Move";
        public const string USER_ADDED = "UserAdded";
        public const string USER_REMOVED = "UserRemoved";
        public const string CREATE_GROUP = "CreateGroup";
        public const string OFFER = "Offer";
        public const string PLAYER_READY = "PlayerReady";
        public const string ANSWER = "Answer";
        public const string CANDIDATE = "Candidate";
        public const string CANDIDATE_TO_GAME = "CandidateToGame";
        public const string BROADCAST_CONNECTION_ID = "BroadcastConnectionId";
        public const string SET_INPUT = "SetInput";

        public Action<string> OnPlayerAdded { get; set; }
        public Action<string> OnPlayerRemoved { get; set; }
        public Action<string> OnPlayerReady { get; set; }
        public Action<string> OnConnectionIdBroadcast { get; set; }
        public Action<OfferModel> OnAnswer { get; set; }
        public Action<CandidateModel> OnCandidate { get; set; }

        public string ConnectionId
        {
            get => _socketClient.ConnectionId;
        }

        private ISocketClient _socketClient = new SignalRClient();

        public async Task InitAsync()
        {
            await _socketClient.InitAsync("v1/game");

            _socketClient.On(USER_ADDED, (userId) => OnPlayerAdded?.Invoke(userId));
            _socketClient.On(USER_REMOVED, (userId) => OnPlayerRemoved?.Invoke(userId));
            _socketClient.On<OfferModel>(ANSWER, description => OnAnswer?.Invoke(description));
            _socketClient.On<CandidateModel>(
                CANDIDATE_TO_GAME,
                candidate => OnCandidate?.Invoke(candidate)
            );
            _socketClient.On(PLAYER_READY, connectionId => OnPlayerReady?.Invoke(connectionId));
            _socketClient.On(
                BROADCAST_CONNECTION_ID,
                connectionId =>
                {
                    OnConnectionIdBroadcast?.Invoke(connectionId);
                }
            );
        }

        public async Task AddToGroup(string code)
        {
            await _socketClient.SendMessage(CREATE_GROUP, code);
        }

        public async Task Offer(OfferModel offer)
        {
            await _socketClient.SendMessage(OFFER, offer);
        }

        public async Task Candidate(CandidateModel candidate)
        {
            await _socketClient.SendMessage(CANDIDATE, candidate);
        }

        public async Task SetInput(SetInputModel model)
        {
            await _socketClient.SendMessage(SET_INPUT, model);
        }
    }
}
