﻿using System;
using System.Threading.Tasks;

namespace Forfeit.EventReceivers
{
    public interface ISocketClient
    {
        string ConnectionId { get; }
        Task InitAsync(string url);
        void On(string methodName, Action<string> handler);
        void On<T1>(string methodName, Action<T1> handler);
        void On<T1>(string methodName, Func<T1, Task> handler);
        Task SendMessage<T1>(string methodName, T1 arg1);
    }
}
