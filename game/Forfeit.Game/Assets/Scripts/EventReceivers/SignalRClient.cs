﻿using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using UnityEngine;
using Forfeit.Utilities;

namespace Forfeit.EventReceivers
{
    public class SignalRClient : ISocketClient
    {
        private const string ROOT_URL = "https://localhost:7087";
        private HubConnection _connection;

        public string ConnectionId { get => _connection.ConnectionId; }

        public async Task InitAsync(string url)
        {
            _connection = new HubConnectionBuilder()
                .WithUrl($"{ROOT_URL}/{url}")
                .WithAutomaticReconnect()
                .Build();

            await StartConnectionAsync();
        }

        public void On(string methodName, Action<string> handler)
        {
            if (_connection != null)
            {
                _connection.On<string>(methodName, (input) =>
                {
                    handler(input);
                });
            }
        }

        public void On<T1>(string methodName, Action<T1> handler)
        {
            if (_connection != null) 
            {
                _connection.On<string>(methodName, (input) =>
                {
                    try
                    {
                        var arg = JsonConvert.DeserializeObject<T1>(input);

                        handler(arg);
                    }
                    catch (Exception ex) when (ex is InvalidCastException || ex is InvalidOperationException)
                    {
                        LoggerService.Error($"Failed parsing input for method: {methodName}");
                    }
                });
            }
        }

        public void On<T1>(string methodName, Func<T1, Task> handler)
        {
            if (_connection != null)
            {
                _connection.On<string>(methodName, async input =>
                {
                    try
                    {
                        var arg = JsonConvert.DeserializeObject<T1>(input);

                        await handler(arg);
                    }
                    catch (Exception ex) when (ex is InvalidCastException || ex is InvalidOperationException)
                    {
                        LoggerService.Error($"Failed parsing input for method: {methodName}");
                    }
                });
            }
        }

        public async Task SendMessage<T1>(string methodName, T1 arg1)
        {
            if (_connection != null)
            {
                await _connection.SendAsync(methodName, arg1);
            }
        }

        private async Task StartConnectionAsync()
        {
            try
            {
                await _connection.StartAsync();

                if (string.IsNullOrEmpty(_connection.ConnectionId) == false)
                {
                    LoggerService.Verbose("Connected to hub.");
                }
            }
            catch (Exception ex)
            {
                LoggerService.Error(ex);
            }
        }
    }
}
