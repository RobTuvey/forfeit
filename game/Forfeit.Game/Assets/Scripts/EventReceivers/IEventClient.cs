﻿using System.Threading.Tasks;

namespace Forfeit.EventReceivers
{
    public interface IEventClient
    {
        string ConnectionId { get; }
        Task InitAsync();
    }
}
