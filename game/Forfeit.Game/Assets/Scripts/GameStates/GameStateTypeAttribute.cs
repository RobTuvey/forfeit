using System;

namespace Forfeit.GameStates
{
    [AttributeUsage(AttributeTargets.Class)]
    public class GameStateTypeAttribute : Attribute
    {
        public Type Type { get; private set; }

        public GameStateTypeAttribute(Type type)
        {
            Type = type;
        }
    }
}
