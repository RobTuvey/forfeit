﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.GameStates
{
    public interface IGameState { }

    public abstract class GameState : MonoBehaviour
    {
        protected GameState Parent;
        protected DiContainer Container;

        protected List<GameState> SubStates = new List<GameState>();

        public virtual void Enter() { }

        public virtual void Exit() { }

        public virtual void Update() { }

        protected TState AddSubState<TState>()
            where TState : GameState
        {
            if (Container != null)
            {
                var result = Container.Resolve<TState>();
                var state = result as GameState;

                if (state != null)
                {
                    var subContainer = Container.CreateSubContainer();
                    var type = GetInterfaceType(typeof(TState));
                    subContainer.Bind(type).To<TState>().FromInstance(result);
                    state.Container = subContainer;
                    state.Parent = this;
                    state.gameObject.transform.parent = transform;

                    SubStates.Add(state);

                    state.Enter();
                }

                return result;
            }

            return default;
        }

        protected TState SwitchSubState<TState>()
            where TState : GameState
        {
            for (int i = SubStates.Count - 1; i >= 0; i--)
            {
                var state = SubStates[i];
                RemoveSubState(state);
            }

            return AddSubState<TState>();
        }

        protected void RemoveSubState(GameState state)
        {
            SubStates.Remove(state);
            state.Exit();
            Destroy(state.gameObject);
        }

        public void UpdateSubStates()
        {
            foreach (GameState state in SubStates)
            {
                state.Update();
            }
        }

        private Type GetInterfaceType(Type stateType) =>
            stateType.GetCustomAttribute<GameStateTypeAttribute>()?.Type;
    }
}
