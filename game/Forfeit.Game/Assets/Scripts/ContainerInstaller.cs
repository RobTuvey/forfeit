using Forfeit.Controllers.DataChannel;
using Forfeit.EventReceivers;
using Forfeit.GameStates;
using Forfeit.Managers;
using Forfeit.Services;
using ModestTree;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Zenject;

public class ContainerInstaller : MonoInstaller
{
    public GameObject GameManagerPrefab;
    public GameObject NetworkManagerPrefab;
    public GameObject LevelManagerPrefab;

    private IGameClient _gameClient;

    public override void InstallBindings()
    {
        _gameClient = new GameClient();

        Container.Bind<IGameClient>().FromInstance(_gameClient).CopyIntoAllSubContainers();
        Container.Bind<DataChannelController>().AsTransient().CopyIntoAllSubContainers();

        BindGameStates(GameManagerPrefab.GetComponent<GameManager>());
        BindServices();
        BindManagers();
    }

    private void BindGameStates(MonoBehaviour root)
    {
        foreach (FieldInfo field in GetGameStates(root))
        {
            var next = field.GetValue(root) as MonoBehaviour;
            if (next != null)
            {
                Container
                    .Bind(field.FieldType)
                    .FromComponentInNewPrefab(next.gameObject)
                    .AsTransient()
                    .CopyIntoAllSubContainers();

                BindGameStates(next);
            }
        }
    }

    private IEnumerable<FieldInfo> GetGameStates(MonoBehaviour root) =>
        root.GetType()
            .GetFields()
            .Where(field => field.FieldType.HasAttribute<GameStateTypeAttribute>());

    private void BindServices()
    {
        Container
            .Bind<ISessionService>()
            .To<SessionService>()
            .AsTransient()
            .CopyIntoAllSubContainers();
        Container.Bind<IUserService>().To<UserService>().AsTransient().CopyIntoAllSubContainers();
        Container.Bind<IHttpService>().To<HttpService>().AsTransient().CopyIntoAllSubContainers();
        Container
            .Bind<INetworkService>()
            .To<NetworkService>()
            .AsTransient()
            .CopyIntoAllSubContainers();
    }

    private void BindManagers()
    {
        GameObject managersContainer = new GameObject("Managers");

        DontDestroyOnLoad(managersContainer);

        BindManager<IGameManager>(GameManagerPrefab, managersContainer);
        BindManager<INetworkManager>(NetworkManagerPrefab, managersContainer);
        BindManager<ILevelManager>(LevelManagerPrefab, managersContainer);
    }

    private void BindManager<TManager>(GameObject manager, GameObject container)
        where TManager : class
    {
        var builder = Container.Bind<TManager>().FromComponentInNewPrefab(manager);

        builder.AsSingle().NonLazy();

        builder.OnInstantiated<MonoBehaviour>(
            (_, instance) =>
            {
                ReparentInstance(instance.gameObject, container);
                Container
                    .Bind<TManager>()
                    .FromInstance(instance as TManager)
                    .MoveIntoAllSubContainers();
            }
        );
    }

    private static void ReparentInstance(GameObject instance, GameObject parent)
    {
        instance.transform.parent = parent.transform;
        instance.gameObject.name = instance.gameObject.name.Replace("(Clone)", "");
    }
}
