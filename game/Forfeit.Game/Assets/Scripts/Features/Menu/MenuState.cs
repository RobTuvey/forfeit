using Forfeit.Features.Menu.MainMenu;
using Forfeit.GameStates;
using Zenject;

namespace Forfeit.Features.Menu
{
    public interface IMenu : IGameState { }

    [GameStateType(typeof(IMenu))]
    public class MenuState : GameState, IMenu
    {
        public MainMenuState MainMenuPrefab;

        private void Start()
        {
            PopToMainMenu();
        }

        public void PopToMainMenu()
        {
            SwitchSubState<MainMenuState>();
        }
    }
}
