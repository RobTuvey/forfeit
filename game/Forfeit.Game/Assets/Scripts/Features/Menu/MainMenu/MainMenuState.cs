using Forfeit.GameStates;
using Forfeit.Utilities;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.Features.Menu.MainMenu
{
    public interface IMainMenu : IGameState { }

    [GameStateType(typeof(IMainMenu))]
    public class MainMenuState : GameState, IMainMenu
    {
        private const string DEBUG_MENU = "DebugMenu";
        private const string PLAY = "Play";

        public UIDocument Document;

        private Button _debugMenuButton;
        private IRootState _root;

        [Inject]
        public void Inject(IRootState root)
        {
            _root = root;
        }

        private void Awake()
        {
            if (Document != null)
            {
                Document.RegisterButtonClick(PLAY, HandlePlayClicked);
                _debugMenuButton = Document.rootVisualElement.Q(DEBUG_MENU) as Button;

#if UNITY_EDITOR
                if (_debugMenuButton != null)
                {
                    _debugMenuButton.style.display = DisplayStyle.Flex;
                    _debugMenuButton.RegisterCallback<ClickEvent>(HandleDebugMenuClicked);
                }
#endif
            }
        }

        private void OnDisable()
        {
            Document.UnregisterButtonClick(PLAY, HandlePlayClicked);
#if UNITY_EDITOR
            _debugMenuButton.UnregisterCallback<ClickEvent>(HandleDebugMenuClicked);
#endif
        }

        private void HandleDebugMenuClicked(ClickEvent _)
        {
            if (_root is not null)
            {
                _root.SwitchToDebugMenu();
            }
        }

        private void HandlePlayClicked(ClickEvent _)
        {
            if (_root is not null)
            {
                _root.SwitchToPlay();
            }
        }
    }
}
