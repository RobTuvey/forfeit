using Forfeit.EventReceivers;
using Forfeit.GameStates;
using Forfeit.Models;
using Forfeit.Utilities;
using System.Collections.Generic;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.Features.Play.Win
{
    public interface IWinState : IGameState { }

    [GameStateType(typeof(IWinState))]
    public class WinState : BasePlayState, IWinState
    {
        private const string RETURN = "Return";
        private const string WINNER = "Winner";

        public UIDocument Document;

        private IPlayState _playState;
        private IGameClient _gameClient;

        [Inject]
        public void Inject(IPlayState playState, IGameClient gameClient)
        {
            _playState = playState;
            _gameClient = gameClient;
        }

        public override void Enter()
        {
            var winner = _playState.GetWinner();
            var session = _playState.GetSession();

            if (winner != null)
            {
                Document.SetLabelText(WINNER, $"{winner.UserName} wins!");
            }

            _gameClient.SetInput(SetInputModel.SetGroupInput(session.Code, new List<InputModel>()));
        }

        private void Awake()
        {
            Document.RegisterButtonClick(RETURN, HandleReturnClicked);
        }

        private void OnDisable()
        {
            Document.UnregisterButtonClick(RETURN, HandleReturnClicked);
        }

        private void HandleReturnClicked(ClickEvent _)
        {
            _playState.ReturnToLobby();
        }
    }
}
