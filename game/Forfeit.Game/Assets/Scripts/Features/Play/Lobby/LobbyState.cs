using Forfeit.Controllers;
using Forfeit.Controllers.DataChannel;
using Forfeit.EventReceivers;
using Forfeit.Features.Play.Games.PedalRace;
using Forfeit.GameStates;
using Forfeit.Models;
using Forfeit.Utilities;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.Features.Play.Lobby
{
    public interface ILobby : IGameState { }

    [GameStateType(typeof(ILobby))]
    public class LobbyState : BasePlayState, ILobby
    {
        private const string SESSION_CODE = "SessionCode";
        private const string PEDAL_RACE = "PedalRace";

        private readonly IReadOnlyList<InputModel> _inputs = new List<InputModel>
        {
            JoystickInputModel.Create().WithRow(2).WithColumn(10).WithSize(10),
            ButtonInputModel
                .CreateWithIcon("arrow-up", "jump")
                .WithRow(20)
                .WithColumn(10)
                .WithSize(8)
        };

        public UIDocument Document;
        public Canvas Canvas;
        public GameObject PlayerPrefab;
        public Transform PlayerParent;
        public Vector3 CameraPosition;
        public Vector3 CameraRotation;

        private IPlayState _playState;
        private DiContainer _container;
        private IGameClient _gameClient;
        private Session _session;
        private Dictionary<string, GameObject> _players = new Dictionary<string, GameObject>();

        [Inject]
        public void Inject(IPlayState playState, DiContainer container, IGameClient gameClient)
        {
            _playState = playState;
            _session = _playState.GetSession();
            _container = container;
            _gameClient = gameClient;
        }

        public override void Enter()
        {
            Initialize();

            if (Document != null)
            {
                if (_session != null)
                {
                    Document.SetLabelText(SESSION_CODE, _session.Code);
                }
            }

            Camera.main.transform.SetLocalPositionAndRotation(
                CameraPosition,
                Quaternion.Euler(CameraRotation)
            );
        }

        private void Awake()
        {
            Document.RegisterButtonClick(PEDAL_RACE, HandlePedalRaceClicked);
        }

        private void OnDisable()
        {
            Document.UnregisterButtonClick(PEDAL_RACE, HandlePedalRaceClicked);
        }

        public override void OnPlayerAdded(User user)
        {
            CreatePlayer(user);

            _gameClient.SetInput(SetInputModel.SetClientInput(user.ConnectionId, _inputs));
        }

        public override void OnPlayerRemoved(string userId)
        {
            if (_players.ContainsKey(userId))
            {
                var player = _players[userId];

                Destroy(player);

                _players.Remove(userId);
            }
        }

        public void HandlePedalRaceClicked(ClickEvent _)
        {
            _playState.StartPlayState<PedalRaceState>();
        }

        private void Initialize()
        {
            var session = _playState.GetSession();
            var players = _playState.GetPlayers();

            foreach (var player in players)
            {
                CreatePlayer(player);
            }

            _gameClient.SetInput(SetInputModel.SetGroupInput(session.Code, _inputs));
        }

        private GameObject CreatePlayer(User user)
        {
            var player = Instantiate(PlayerPrefab, PlayerParent);

            PlayerController.SetUp(player, user);

            DataChannelController dataChannel =
                _container.InstantiateComponent<DataChannelController>(player);
            player.AddComponent<DataChannelInputController>();

            HoveringLabelController.SetupLabel(player, user, Canvas);

            var playerDataChannelClient = new PlayerDataChannelClient(
                user.ConnectionId,
                _gameClient,
                dataChannel
            );

            dataChannel.SetClient(playerDataChannelClient);

            StartCoroutine(dataChannel.CreateOffer());

            _players.Add(user.Id.ToString(), player);

            return player;
        }
    }
}
