using Forfeit.GameStates;
using Forfeit.Models;
using UnityEngine;

namespace Forfeit.Features.Play
{
    public abstract class BasePlayState : GameState
    {
        public virtual void OnPlayerAdded(User user) { }

        public virtual void OnPlayerRemoved(string userId) { }

        protected void WithCameraPosition(Vector3 cameraPosition, Vector3 cameraRotation) =>
            Camera.main.transform.SetLocalPositionAndRotation(
                cameraPosition,
                Quaternion.Euler(cameraRotation)
            );
    }
}
