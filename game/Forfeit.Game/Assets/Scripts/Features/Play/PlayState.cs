using Forfeit.EventReceivers;
using Forfeit.Features.Play.Games.PedalRace;
using Forfeit.Features.Play.Lobby;
using Forfeit.Features.Play.Win;
using Forfeit.GameStates;
using Forfeit.Models;
using Forfeit.Services;
using Forfeit.Utilities;
using System.Collections.Generic;
using System.Linq;
using Zenject;

namespace Forfeit.Features.Play
{
    public interface IPlayState : IGameState
    {
        Session GetSession();

        IReadOnlyList<User> GetPlayers();

        void ReturnToLobby();

        void SetWinner(string userId);

        User GetWinner();

        void ShowResults();

        void StartPlayState<TPlay>()
            where TPlay : BasePlayState;
    }

    [GameStateType(typeof(IPlayState))]
    public class PlayState : GameState, IPlayState
    {
        public LobbyState LobbyPrefab;
        public WinState WinPrefab;

        #region REGION: Games

        public PedalRaceState PedalRacePrefab;

        #endregion REGION: Games

        private ISessionService _sessionService;
        private IUserService _userService;
        private IGameClient _gameClient;
        private Dictionary<string, User> _players = new Dictionary<string, User>();
        private string _winnerId;

        public Session Session { get; private set; }

        [Inject]
        public void Inject(
            ISessionService sessionService,
            IGameClient gameClient,
            IUserService userService
        )
        {
            _sessionService = sessionService;
            _gameClient = gameClient;
            _userService = userService;
        }

        public IReadOnlyList<User> GetPlayers() => _players.Values.ToList();

        public Session GetSession() => Session;

        private void Awake()
        {
            _gameClient.OnPlayerAdded += HandlePlayerAdded;
            _gameClient.OnPlayerRemoved += HandlePlayerRemoved;
            if (Session is null)
            {
                StartCoroutine(_sessionService.CreateSession(HandleSessionCreated));
            }
        }

        private void OnDisable()
        {
            _gameClient.OnPlayerAdded -= HandlePlayerAdded;
            _gameClient.OnPlayerRemoved -= HandlePlayerRemoved;
        }

        private void HandleSessionCreated(Session session)
        {
            LoggerService.Info($"Session created '{session.Id}'");

            Session = session;

            SwitchSubState<LobbyState>();
        }

        private void HandlePlayerAdded(string userId)
        {
            StartCoroutine(_userService.GetUserById(userId, HandlePlayerLoaded));
        }

        private void HandlePlayerRemoved(string userId)
        {
            LoggerService.Info($"Player left: {userId}");

            if (_players.ContainsKey(userId))
            {
                _players.Remove(userId);

                foreach (var state in SubStates.OfType<BasePlayState>())
                {
                    state.OnPlayerRemoved(userId);
                }
            }
        }

        private void HandlePlayerLoaded(User player)
        {
            _players.Add(player.Id.ToString(), player);

            foreach (var state in SubStates.OfType<BasePlayState>())
            {
                state.OnPlayerAdded(player);
            }
        }

        public void StartPlayState<TPlay>()
            where TPlay : BasePlayState
        {
            SwitchSubState<TPlay>();
        }

        public void ReturnToLobby()
        {
            SwitchSubState<LobbyState>();
        }

        public void SetWinner(string userId)
        {
            _winnerId = userId;
        }

        public void ShowResults()
        {
            SwitchSubState<WinState>();
        }

        public User GetWinner()
        {
            if (string.IsNullOrEmpty(_winnerId) || _players.ContainsKey(_winnerId) == false)
            {
                return default;
            }

            return _players[_winnerId];
        }
    }
}
