using UnityEngine;
using Zenject;

namespace Forfeit.Features.Play.Games.PedalRace.Controllers
{
    public class PedalRaceFinishController : MonoBehaviour
    {
        public PedalRaceState PedalRaceState;

        private bool _handled = false;

        private void OnTriggerEnter(Collider other)
        {
            var playerController = other.GetComponent<PedalRacePlayerController>();

            if (_handled == false && playerController != null && PedalRaceState != null)
            {
                PedalRaceState.HandleWin(playerController.User);

                _handled = true;
            }
        }
    }
}
