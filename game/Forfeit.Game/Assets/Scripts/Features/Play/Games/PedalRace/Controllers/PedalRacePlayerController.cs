using Forfeit.Models;
using Forfeit.Utilities;
using UnityEngine;
using Zenject;

namespace Forfeit.Features.Play.Games.PedalRace.Controllers
{
    public class PedalRacePlayerController : MonoBehaviour
    {
        public User User;
        public Renderer Renderer;
        public float Power;
        public float MaxMagnitude = 10.0f;
        public string Next;
        public Animator Animator;

        private IPedalRaceState _pedalRaceState;
        private InputController _inputController;
        private Rigidbody _rigidbody;

        [Inject]
        public void Inject(IPedalRaceState pedalRaceState)
        {
            _pedalRaceState = pedalRaceState;
        }

        private void Awake()
        {
            _inputController = GetComponent<InputController>();
            _rigidbody = GetComponent<Rigidbody>();

            _inputController.RegisterDownCallback("left", HandleLeftPressed);
            _inputController.RegisterDownCallback("right", HandleRightPressed);
        }

        private void Start()
        {
            RendererUtilities.SetModelToRandomColour(gameObject);
        }

        private void OnDrawGizmos()
        {
            UnityEngine.Debug.DrawRay(transform.position, transform.forward, Color.white);
        }

        private void HandleStep(string step)
        {
            if (string.IsNullOrEmpty(Next) || Next != step)
            {
                Next = step;

                _rigidbody.AddForce(transform.forward * Power);

                var velocity = _rigidbody.velocity;
                var magnitude = Mathf.Min(MaxMagnitude, velocity.magnitude);
                var forward = magnitude / MaxMagnitude;

                LoggerService.Info($"Magnitude: {magnitude}");

                Animator.SetFloat("Forward", forward);
            }
        }

        private void HandleLeftPressed() => HandleStep("left");

        private void HandleRightPressed() => HandleStep("right");
    }
}
