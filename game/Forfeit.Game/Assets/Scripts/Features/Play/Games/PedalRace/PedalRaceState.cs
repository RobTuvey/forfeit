using Forfeit.Controllers;
using Forfeit.Controllers.DataChannel;
using Forfeit.EventReceivers;
using Forfeit.Features.Play.Games.PedalRace.Controllers;
using Forfeit.GameStates;
using Forfeit.Models;
using Forfeit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.Features.Play.Games.PedalRace
{
    public interface IPedalRaceState : IGameState
    {
        void HandleWin(User user);
    }

    [GameStateType(typeof(IPedalRaceState))]
    public class PedalRaceState : BasePlayState, IPedalRaceState
    {
        private const float PLAYER_GAP = 2.0f;
        private const string BACK = "Back";

        private readonly IReadOnlyList<InputModel> _inputs = new List<InputModel>
        {
            ButtonInputModel
                .CreateWithIcon("arrow-left", "left")
                .WithColumn(6)
                .WithRow(4)
                .WithSize(12),
            ButtonInputModel
                .CreateWithIcon("arrow-right", "right")
                .WithColumn(6)
                .WithRow(18)
                .WithSize(12)
        };

        public UIDocument Document;
        public GameObject PlayerPrefab;
        public Transform PlayerContainer;
        public Vector3 CameraPosition;
        public Vector3 CameraRotation;
        public Canvas Canvas;

        private IPlayState _playState;
        private IGameClient _gameClient;

        private Dictionary<Guid, GameObject> _players = new Dictionary<Guid, GameObject>();

        [Inject]
        public void Inject(IPlayState playState, IGameClient gameClient)
        {
            _playState = playState;
            _gameClient = gameClient;
        }

        public override void Enter()
        {
            Container.Bind<PedalRacePlayerController>().AsTransient();
            Container.Bind<PedalRaceFinishController>().AsTransient();

            WithCameraPosition(CameraPosition, CameraRotation);

            Initialize();
        }

        private void Awake()
        {
            Document.RegisterButtonClick(BACK, HandleBackClicked);
        }

        private void OnDisable()
        {
            Document.UnregisterButtonClick(BACK, HandleBackClicked);
        }

        public void HandleBackClicked(ClickEvent _)
        {
            _playState.ReturnToLobby();
        }

        public void HandleWin(User user)
        {
            _playState.SetWinner(user.Id.ToString());
            _playState.ShowResults();
        }

        private void Initialize()
        {
            _players = new Dictionary<Guid, GameObject>();

            var session = _playState.GetSession();
            var players = _playState.GetPlayers();

            _gameClient.SetInput(SetInputModel.SetGroupInput(session.Code, _inputs));

            foreach (var player in players)
            {
                var pedalPlayer = CreatePlayer(player);

                _players.Add(player.Id, pedalPlayer);
            }

            ArrangePlayers();
        }

        private void ArrangePlayers()
        {
            var playersArray = _players.ToArray();
            for (int i = 0; i < _players.Count; i++)
            {
                var (_, player) = playersArray[i];
                var rigidbody = player.GetComponent<Rigidbody>();
                var constraints = rigidbody.constraints;
                rigidbody.constraints = RigidbodyConstraints.None;

                player.transform.localPosition = new Vector3(0, 0, PLAYER_GAP * i);

                rigidbody.constraints = constraints;
            }

            var containerPosition = (PLAYER_GAP * Mathf.Max(0, playersArray.Length - 1)) / 2;
            PlayerContainer.localPosition = new Vector3(
                PlayerContainer.localPosition.x,
                0,
                -containerPosition
            );
        }

        private GameObject CreatePlayer(User user)
        {
            var player = Container.InstantiatePrefab(PlayerPrefab, PlayerContainer);

            var controller = player.GetComponent<PedalRacePlayerController>();

            DataChannelController dataChannel =
                Container.InstantiateComponent<DataChannelController>(player);
            player.AddComponent<DataChannelInputController>();

            HoveringLabelController.SetupLabel(player, user, Canvas);

            var playerDataChannelClient = new PlayerDataChannelClient(
                user.ConnectionId,
                _gameClient,
                dataChannel
            );

            dataChannel.SetClient(playerDataChannelClient);

            StartCoroutine(dataChannel.CreateOffer());

            controller.User = user;

            return player;
        }
    }
}
