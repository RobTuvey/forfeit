using Forfeit.Controllers.DataChannel;
using Forfeit.EventReceivers;
using Forfeit.GameStates;
using Forfeit.Utilities;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.Features.Debug.Network
{
    public interface INetworkState : IGameState { }

    [GameStateType(typeof(INetworkState))]
    public class NetworkState : GameState, INetworkState
    {
        private const string BACK_BUTTON = "Back";
        private const string RESET_BUTTON = "Reset";

        public DataChannelController DataChannelController;
        public UIDocument Document;

        private IGameClient _gameClient;
        private IDebugState _debugState;
        private NetworkStateDataChannelClient _dataChannelClient;

        [Inject]
        public void Inject(IGameClient gameClient, IDebugState debugState)
        {
            _debugState = debugState;
            _gameClient = gameClient;

            _dataChannelClient = new NetworkStateDataChannelClient(
                gameClient,
                DataChannelController,
                Document
            );
            DataChannelController.SetClient(_dataChannelClient);
        }

        private void Awake()
        {
            _gameClient.OnConnectionIdBroadcast += HandleConnectionId;

            Document.RegisterButtonClick(BACK_BUTTON, HandleBackClicked);
            Document.RegisterButtonClick(RESET_BUTTON, HandleResetClicked);
        }

        private void OnDisable()
        {
            _gameClient.OnConnectionIdBroadcast -= HandleConnectionId;

            Document.UnregisterButtonClick(BACK_BUTTON, HandleBackClicked);
            Document.UnregisterButtonClick(RESET_BUTTON, HandleResetClicked);
        }

        private void HandleConnectionId(string connectionId)
        {
            _dataChannelClient.HandleConnectionId(connectionId);

            StartCoroutine(DataChannelController.CreateOffer());
        }

        private void HandleBackClicked(ClickEvent _)
        {
            _debugState?.PopToMenu();
        }

        private void HandleResetClicked(ClickEvent _)
        {
            DataChannelController.ResetDataChannel();
            StartCoroutine(DataChannelController.CreateOffer());
            _dataChannelClient.Reset();
        }
    }
}
