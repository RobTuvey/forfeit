using Forfeit.Controllers.DataChannel;
using Forfeit.EventReceivers;
using Forfeit.Models;
using Forfeit.Utilities;
using Unity.WebRTC;
using UnityEngine.UIElements;

namespace Forfeit.Features.Debug.Network
{
    public class NetworkStateDataChannelClient : DataChannelClient, IDataChannelClient
    {
        private const string STATUS_LABEL = "Status";
        private const string GATHERING_LABEL = "Gathering";
        private const string FEED_LABEL = "Feed";

        private readonly IGameClient _gameClient;
        private readonly DataChannelController _dataChannel;
        private readonly UIDocument _document;

        private string _connectionId = string.Empty;

        public NetworkStateDataChannelClient(
            IGameClient gameClient,
            DataChannelController dataChannel,
            UIDocument document
        )
        {
            _gameClient = gameClient;
            _dataChannel = dataChannel;
            _document = document;

            _gameClient.OnAnswer += HandleAnswer;
            _gameClient.OnCandidate += HandlePeerCandidate;
        }

        public override void OnNegotiationNeeded()
        {
            LoggerService.Info("[Network] Negotiation needed!");

            _document.AppendLabel(FEED_LABEL, "Negotiation needed");
        }

        public override void OnIceGatheringStateChanged(RTCIceGatheringState state)
        {
            LoggerService.Info($"[Network] Gathering state changed: {state}");

            _document.SetLabelText(GATHERING_LABEL, $"Gathering: {state}");
        }

        public override void OnOffer(OfferModel offer)
        {
            LoggerService.Info("[Network] Sending offer");

            offer.TargetConnectionId = _connectionId;
            offer.ConnectionId = _gameClient.ConnectionId;

            _document.AppendLabel(FEED_LABEL, "Sending offer");
            _gameClient.Offer(offer);
        }

        public override void OnCandidate(CandidateModel candidate)
        {
            LoggerService.Info("[Network] Sending candidate");

            candidate.TargetConnectionId = _connectionId;
            candidate.ConnectionId = _gameClient.ConnectionId;

            _gameClient.Candidate(candidate);
        }

        public override void OnConnectionStateChanged(RTCPeerConnectionState state)
        {
            LoggerService.Info($"[Network] Connection state: {state}");

            _document.SetLabelText(STATUS_LABEL, $"Status: {state}");
        }

        public void HandleConnectionId(string connectionId)
        {
            LoggerService.Info($"[Network] Connection ID received: {connectionId}");

            _document.AppendLabel(FEED_LABEL, $"Connection ID received: {connectionId}");
            _connectionId = connectionId;
        }

        public void Reset()
        {
            _document.SetLabelText(STATUS_LABEL, "Status: ");
            _document.SetLabelText(GATHERING_LABEL, "Gathering: ");
            _document.SetLabelText(FEED_LABEL, "Feed: ");
        }

        private void HandleAnswer(OfferModel offer)
        {
            LoggerService.Info("[Network] Answer received");

            _document.AppendLabel(FEED_LABEL, "Answer received");
            _dataChannel.HandleAnswer(offer);
        }

        private void HandlePeerCandidate(CandidateModel candidate)
        {
            LoggerService.Info("[Network] Candidate received");

            _document.AppendLabel(FEED_LABEL, "Candidate received");
            _dataChannel.HandlePeerCandidate(candidate);
        }
    }
}
