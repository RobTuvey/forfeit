using Forfeit.GameStates;
using Forfeit.Utilities;
using UnityEngine.UIElements;
using Zenject;

namespace Forfeit.Features.Debug
{
    public interface IDebugMenu : IGameState { }

    [GameStateType(typeof(IDebugMenu))]
    public class DebugMenuState : GameState, IDebugMenu
    {
        private const string BACK_BUTTON = "Back";
        private const string NETWORK_BUTTON = "Network";
        private const string NETWORK_PLAYER_BUTTON = "NetworkPlayer";

        public UIDocument Document;

        private IRootState _rootState;
        private IDebugState _debugState;

        [Inject]
        public void Inject(IRootState rootState, IDebugState debugState)
        {
            _rootState = rootState;
            _debugState = debugState;
        }

        private void Awake()
        {
            RegisterCallbacks();
        }

        private void OnDisable()
        {
            UnregisterCallbacks();
        }

        private void RegisterCallbacks()
        {
            Document.RegisterButtonClick(BACK_BUTTON, OnBackButtonClicked);
            Document.RegisterButtonClick(NETWORK_BUTTON, OnNetworkButtonClicked);
            Document.RegisterButtonClick(NETWORK_PLAYER_BUTTON, OnNetworkPlayerButtonClicked);
        }

        private void UnregisterCallbacks()
        {
            Document.UnregisterButtonClick(BACK_BUTTON, OnBackButtonClicked);
            Document.UnregisterButtonClick(NETWORK_BUTTON, OnNetworkButtonClicked);
            Document.UnregisterButtonClick(NETWORK_PLAYER_BUTTON, OnNetworkPlayerButtonClicked);
        }

        private void OnBackButtonClicked(ClickEvent _)
        {
            _rootState?.PopToMenu();
        }

        private void OnNetworkButtonClicked(ClickEvent _)
        {
            _debugState?.OpenNetworkDebug();
        }

        private void OnNetworkPlayerButtonClicked(ClickEvent _)
        {
            _debugState?.OpenNetworkPlayerDebug();
        }
    }
}
