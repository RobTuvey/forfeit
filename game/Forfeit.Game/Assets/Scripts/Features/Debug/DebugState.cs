using Forfeit.Features.Debug;
using Forfeit.Features.Debug.Network;
using Forfeit.GameStates;

public interface IDebugState : IGameState
{
    void PopToMenu();

    void OpenNetworkDebug();

    void OpenNetworkPlayerDebug();
}

[GameStateType(typeof(IDebugState))]
public class DebugState : GameState, IDebugState
{
    public DebugMenuState DebugMenuPrefab;
    public NetworkState NetworkStatePrefab;
    public NetworkPlayerState NetworkPlayerPrefab;

    private void Start()
    {
        PopToMenu();
    }

    public void PopToMenu()
    {
        SwitchSubState<DebugMenuState>();
    }

    public void OpenNetworkDebug()
    {
        SwitchSubState<NetworkState>();
    }

    public void OpenNetworkPlayerDebug()
    {
        SwitchSubState<NetworkPlayerState>();
    }
}
