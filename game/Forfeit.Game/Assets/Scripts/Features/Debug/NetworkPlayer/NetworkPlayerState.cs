using Forfeit.Controllers.DataChannel;
using Forfeit.EventReceivers;
using Forfeit.GameStates;
using Forfeit.Utilities;
using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

public interface INetworkPlayerstate : IGameState { }

[GameStateType(typeof(INetworkPlayerstate))]
public class NetworkPlayerState : GameState, INetworkPlayerstate
{
    private const string BACK_BUTTON = "Back";

    public GameObject Player;
    public Vector3 CameraPosition = Vector3.zero;
    public Vector3 CameraRotation = Vector3.zero;
    public UIDocument Document;

    private IDebugState _debugState;
    private IGameClient _gameClient;
    private DiContainer _container;
    private DataChannelController _playerDataChannel;
    private GameObject _player;

    [Inject]
    public void Inject(IGameClient gameClient, IDebugState debugState, DiContainer container)
    {
        _gameClient = gameClient;
        _debugState = debugState;
        _container = container;
    }

    private void Start()
    {
        Camera.main.transform.SetPositionAndRotation(
            CameraPosition,
            Quaternion.Euler(CameraRotation)
        );

        _player = Instantiate(Player, transform);
        _playerDataChannel = _container.InstantiateComponent<DataChannelController>(_player);
        _player.AddComponent<DataChannelInputController>();
    }

    private void Awake()
    {
        _gameClient.OnConnectionIdBroadcast += HandleConnectionId;

        Document.RegisterButtonClick(BACK_BUTTON, HandleBackButtonPressed);
    }

    private void OnDisable()
    {
        _gameClient.OnConnectionIdBroadcast -= HandleConnectionId;

        Document.UnregisterButtonClick(BACK_BUTTON, HandleBackButtonPressed);
    }

    private void HandleConnectionId(string connectionId)
    {
        var playerDataChannelClient = new PlayerDataChannelClient(
            connectionId,
            _gameClient,
            _playerDataChannel
        );

        _playerDataChannel.SetClient(playerDataChannelClient);

        StartCoroutine(_playerDataChannel.CreateOffer());
    }

    private void HandleBackButtonPressed(ClickEvent _)
    {
        _debugState?.PopToMenu();
    }
}
