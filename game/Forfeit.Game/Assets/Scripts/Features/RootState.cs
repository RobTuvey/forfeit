﻿using Forfeit.Features.Menu;
using Forfeit.Features.Play;
using Forfeit.GameStates;
using UnityEngine;
using Zenject;

namespace Forfeit.Features
{
    public interface IRootState : IGameState
    {
        GameObject GameObject { get; }

        void SwitchToDebugMenu();

        void PopToMenu();

        void SwitchToPlay();
    }

    [GameStateType(typeof(IRootState))]
    public class RootState : GameState, IRootState
    {
        public MenuState MainMenuStatePrefab;
        public DebugState DebugStatePrefab;
        public PlayState PlayStatePrefab;

        public GameObject GameObject
        {
            get => gameObject;
        }

        [Inject]
        public void Inject(DiContainer container)
        {
            Container = container;
        }

        private void Start()
        {
            PopToMenu();
        }

        public void PopToMenu()
        {
            SwitchSubState<MenuState>();
        }

        public void SwitchToDebugMenu()
        {
            SwitchSubState<DebugState>();
        }

        public void SwitchToPlay()
        {
            SwitchSubState<PlayState>();
        }
    }
}
