using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Forfeit.Controllers
{
    public class GameController : MonoBehaviour
    {
        #region REGION: Public Properties

        public Vector3 _cameraPosition = Vector3.zero;
        public Vector3 _cameraRotation = Vector3.zero;

        #endregion

        #region REGION: Private Fields

        private GameObject _camera;

        #endregion

        public void Awake()
        {
            _camera = Camera.main.gameObject;
        }

        public void Start()
        {
            if (_camera != null)
            {
                _camera.transform.SetPositionAndRotation(_cameraPosition, Quaternion.Euler(_cameraRotation.x, _cameraRotation.y, _cameraRotation.z));
            }
        }
    }
}