using Forfeit.Utilities;
using UnityEngine;

namespace Forfeit.Controllers
{
    public class MovementController : MonoBehaviour
    {
        #region REGION: Public Properties

        public float     Speed;
        public float     RotateSpeed = 0.25f;
        public Animator  Animator;
        public Rigidbody PlayerRigidbody;
        public float     JumpForce;

        #endregion

        #region REGION: Private Fields

        private float       _blendSpeed     = 10.0f;
        private float       _animationBlend = 0.0f;
        private Vector2     _moveVector     = Vector2.zero;
        private BoxCollider _boxCollider;
        private InputController _inputController;

        #endregion

        #region REGION: Constants 

        private const float GROUND_DISTANCE = 0.6f;

        #endregion

        private void Start()
        {
            _boxCollider = GetComponent<BoxCollider>();
            _inputController = GetComponent<InputController>();
            _inputController.RegisterOnMoveCallback(SetMoveVector);
            _inputController.RegisterDownCallback("jump", Jump);
        }

        public void SetMoveVector(Vector2 vector)
        {
            if (vector == null)
            {
                _moveVector = Vector2.zero;
            }
            else
            {
                _moveVector = vector;
            }
        }

        public void Jump()
        {
            if (PlayerRigidbody != null && IsGrounded())
            {
                Animator.Play("Jump");
                PlayerRigidbody.AddForce(new Vector3(0, JumpForce, 0));
            }
        }

        private void FixedUpdate()
        {
            Move();

            bool isGrounded = IsGrounded();
            Animator.SetBool("Grounded", isGrounded);
        }

        private void Move()
        {
            var movement = Vector3.zero;
            var strafeAngle = 0.0f;

            if (_moveVector != default(Vector2))
            {
                movement = new Vector3(_moveVector.x, 0, _moveVector.y);
                strafeAngle = Vector3.Angle(transform.forward, movement);
                var rotation = Quaternion.LookRotation(movement);
                rotation = Quaternion.Slerp(PlayerRigidbody.rotation, rotation, RotateSpeed);

                SetRotation(rotation);
                PlayerRigidbody.MovePosition(transform.position + (transform.forward * movement.magnitude * Speed));
            }

            _animationBlend = Mathf.Lerp(_animationBlend, movement.magnitude, _blendSpeed);
            SetAnimatorFloat("Forward", _animationBlend);
            SetAnimatorFloat("Strafe", Mathf.Clamp(strafeAngle / 90, 0, 1));
        }

        private void SetRotation(Quaternion rotation)
        {
            if (PlayerRigidbody != null)
            {
                PlayerRigidbody.MoveRotation(rotation);
            }
        }

        private void SetAnimatorFloat(string name, float value)
        {
            if (Animator != null)
            {
                Animator.SetFloat(name, value);
            }
        }

        private bool IsGrounded()
        {
            var mask = LayerMask.NameToLayer("Ground");
            var start = transform.position + (transform.up * (_boxCollider.size.y / 2));
            return Physics.Raycast(start, -transform.up, GROUND_DISTANCE, 1 << mask);
        }
    }
}