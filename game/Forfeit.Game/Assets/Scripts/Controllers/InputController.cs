using Forfeit.Models.Events;
using Forfeit.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputController : MonoBehaviour
{
    private readonly ICollection<InputCallback> _downCallbacks = new List<InputCallback>();
    private readonly ICollection<InputCallback> _upCallbacks = new List<InputCallback>();
    private readonly ICollection<string> _downInputs = new List<string>();
    private Action<Vector2> _onMove;

    public void RegisterDownCallback(string input, Action action)
        => RegisterCallback(input, action, _downCallbacks);

    public void RegisterUpCallback(string input, Action action)
        => RegisterCallback(input, action, _upCallbacks);

    public void RegisterOnMoveCallback(Action<Vector2> action)
    {
        _onMove += action;
    }

    public void HandleButtonDown(InputEvent inputEvent)
    {
        LoggerService.Verbose($"[OnButtonDown] Input: {inputEvent.Input}");
        if (_downInputs.Contains(inputEvent.Input) == false)
        {
            _downInputs.Add(inputEvent.Input);
        }

        var callback = _downCallbacks.FirstOrDefault(x => x.Input == inputEvent.Input);
        if (callback != null)
        {
            callback.Invoke();
        }
    }

    public void HandleButtonUp(InputEvent inputEvent)
    {
        LoggerService.Verbose($"[OnButtonUp] Input: {inputEvent.Input}");
        if (_downInputs.Contains(inputEvent.Input))
        {
            _downInputs.Remove(inputEvent.Input);
        }

        var callback = _upCallbacks.FirstOrDefault(x => x.Input == inputEvent.Input);
        if (callback != null)
        {
            callback.Invoke();
        }
    }

    public void HandleMove(Vector2 vector)
    {
        _onMove?.Invoke(vector);
    }

    private void RegisterCallback(string input, Action action, ICollection<InputCallback> callbacks)
    {
        if (string.IsNullOrEmpty(input) == false)
        {
            var callback = callbacks.FirstOrDefault(x => x.Input == input);

            if (callback == null)
            {
                callbacks.Add(new InputCallback(input, action));
            }
            else
            {
                callback.Action += action;
            }
        }
    }

    private class InputCallback
    {
        public string Input { get; private set; } = string.Empty;
        public Action Action { get; set; }

        public InputCallback(string input, Action action)
        {
            Input = input;
            Action = action;
        }

        public void Invoke()
        {
            Action?.Invoke();
        }
    }    
}
