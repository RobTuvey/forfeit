﻿using Forfeit.Models;
using Forfeit.Utilities;
using TMPro;
using UnityEngine;

namespace Forfeit.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        public GameObject Model;

        public User User { get; set; }

        private void Start()
        {
            if (Model != null)
            {
                RendererUtilities.SetModelToRandomColour(Model);
            }
        }

        public static PlayerController SetUp(GameObject gameObject, User user)
        {
            var controller = gameObject.GetComponent<PlayerController>();

            if (controller == null)
            {
                LoggerService.Warning("Player contoller not found.");
            }
            else
            {
                controller.User = user;
            }

            return controller;
        }
    }
}
