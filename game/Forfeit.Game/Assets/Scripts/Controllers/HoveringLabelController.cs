using Forfeit.Models;
using Forfeit.Utilities;
using TMPro;
using UnityEngine;

namespace Forfeit.Controllers
{
    public class HoveringLabelController : MonoBehaviour
    {
        public GameObject LabelPrefab;
        public GameObject Label;
        public Canvas Canvas;
        public string Text;
        public float LabelOffset = 1.25f;

        private void Start()
        {
            CreateLabel();
        }

        private void Update()
        {
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            if (Label == null || Canvas == null)
            {
                return;
            }

            var offsetY = transform.position.y + LabelOffset;
            var labelWorldPosition = new Vector3(
                transform.position.x,
                offsetY,
                transform.position.z
            );

            Vector2 canvasPosition;
            Vector2 screenPoint = Camera.main.WorldToScreenPoint(labelWorldPosition);

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                Canvas.transform as RectTransform,
                screenPoint,
                null,
                out canvasPosition
            );

            if (Label.transform is RectTransform labelRectTransform)
            {
                labelRectTransform.localPosition = canvasPosition;
            }
        }

        private void CreateLabel()
        {
            if (Canvas == null)
            {
                return;
            }

            if (LabelPrefab == null)
            {
                return;
            }

            var label = Instantiate(LabelPrefab, Canvas.transform);

            var text = label.GetComponent<TextMeshProUGUI>();
            if (text != null)
            {
                text.text = Text;
            }

            Label = label;
        }

        public static void SetupLabel(GameObject gameObject, User user, Canvas canvas)
        {
            var label = gameObject.GetComponent<HoveringLabelController>();

            if (label == null)
            {
                LoggerService.Warning("Label component not found");
            }
            else
            {
                label.Text = user.UserName;
                label.Canvas = canvas;
            }
        }

        private void OnDestroy()
        {
            Destroy(Label);
        }
    }
}
