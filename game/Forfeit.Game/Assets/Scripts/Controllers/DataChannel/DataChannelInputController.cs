using Forfeit.Controllers;
using Forfeit.Controllers.DataChannel;
using Forfeit.Models;
using Forfeit.Models.Events;
using Forfeit.Utilities;
using Newtonsoft.Json;
using UnityEngine;

namespace Forfeit.Controllers.DataChannel
{
    public class DataChannelInputController : MonoBehaviour
    {
        private InputController _inputController;
        private DataChannelController _dataChannelController;

        private void Awake()
        {
            _inputController = GetComponent<InputController>();
            _dataChannelController = GetComponent<DataChannelController>();

            if (_dataChannelController is not null)
            {
                _dataChannelController.MessageReceived += HandleDataChannelMessage;
            }
        }

        private void OnDisable()
        {
            if (_dataChannelController is not null)
            {
                _dataChannelController.MessageReceived -= HandleDataChannelMessage;
            }
        }

        private void HandleDataChannelMessage(DataChannelMessage message)
        {
            LoggerService.Info($"Message: {JsonConvert.SerializeObject(message)}");
            if (_inputController)
            {
                if (message.type == DataChannelMessage.BUTTON_DOWN)
                {
                    InputEvent buttonDownInputEvent = JsonConvert.DeserializeObject<InputEvent>(
                        message.data.ToString()
                    );
                    LoggerService.Verbose($"[Button Down] {buttonDownInputEvent.Input}");
                    _inputController.HandleButtonDown(buttonDownInputEvent);
                }
                else if (message.type == DataChannelMessage.BUTTON_UP)
                {
                    InputEvent buttonUpInputEvent = JsonConvert.DeserializeObject<InputEvent>(
                        message.data.ToString()
                    );
                    LoggerService.Verbose($"[Button Up] {buttonUpInputEvent.Input}");
                    _inputController.HandleButtonUp(buttonUpInputEvent);
                }
                else if (message.type == DataChannelMessage.MOVE)
                {
                    Vector2 vector = JsonConvert.DeserializeObject<Vector2>(
                        message.data.ToString()
                    );
                    LoggerService.Verbose($"[Move] X: {vector.x} Y: {vector.y}");
                    _inputController.HandleMove(vector);
                }
            }
            else
            {
                LoggerService.Warning("No input controller on player");
            }
        }
    }
}
