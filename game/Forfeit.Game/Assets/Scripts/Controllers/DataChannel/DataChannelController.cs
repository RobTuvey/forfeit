using Forfeit.Managers;
using Forfeit.Models;
using Forfeit.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using Unity.WebRTC;
using UnityEngine;
using Zenject;

namespace Forfeit.Controllers.DataChannel
{
    public class DataChannelController : MonoBehaviour
    {
        public Action<DataChannelMessage> MessageReceived { get; set; }
        public Action<OfferModel> OnAnswer { get; set; }

        private IceResource _settings;
        private RTCPeerConnection _peerConnection;
        private RTCDataChannel _dataChannel;
        private DelegateOnMessage _onDataChannelMessage;
        private IDataChannelClient _client;

        [Inject]
        public void Inject(INetworkManager networkManager)
        {
            _settings = networkManager.TurnSettings;
            HandleTurnSettings();
        }

        private void Awake()
        {
            _onDataChannelMessage += HandleDataChannelMessage;

            if (_client != null)
            {
                _client.Awake();
            }
        }

        private void OnDisable()
        {
            _onDataChannelMessage -= HandleDataChannelMessage;

            if (_client != null)
            {
                _client.Disable();
            }
        }

        public void SetClient(IDataChannelClient client)
        {
            _client = client;

            if (client != null)
            {
                client.Awake();
            }
        }

        public void HandleAnswer(OfferModel answer)
        {
            RTCSessionDescription remoteDescription = new RTCSessionDescription
            {
                sdp = answer.Description.Sdp,
                type = MapType(answer.Description.Type)
            };

            StartCoroutine(SetRemote(remoteDescription));
        }

        public void HandlePeerCandidate(CandidateModel candidate)
        {
            var init = new RTCIceCandidateInit()
            {
                candidate = candidate.Candidate,
                sdpMid = candidate.SdpMid,
                sdpMLineIndex = candidate.SdpMLineIndex
            };
            _peerConnection.AddIceCandidate(new RTCIceCandidate(init));
        }

        public IEnumerator CreateOffer()
        {
            var offer = _peerConnection.CreateOffer();
            yield return offer;

            if (!offer.IsError)
            {
                var local = offer.Desc;
                yield return _peerConnection.SetLocalDescription(ref local);
                OfferModel model = new OfferModel("offer", local.sdp);
                _client.OnOffer(model);
            }
            else
            {
                LoggerService.Warning($"Failed to create offer: ${offer.Error}");
            }
        }

        public void ResetDataChannel()
        {
            _dataChannel.Close();
            _peerConnection.Close();

            HandleTurnSettings();
        }

        private void HandleTurnSettings()
        {
            RTCConfiguration configuration = GetSelectedSdpSemantics();
            _peerConnection = new RTCPeerConnection(ref configuration);
            _peerConnection.OnConnectionStateChange += state =>
                _client.OnConnectionStateChanged(state);
            _peerConnection.OnIceCandidate += HandleIceCandidate;
            _peerConnection.OnNegotiationNeeded += HandleNegotiationNeeded;
            _peerConnection.OnIceGatheringStateChange += state =>
                _client.OnIceGatheringStateChanged(state);

            _dataChannel = _peerConnection.CreateDataChannel("data", new RTCDataChannelInit());
            _dataChannel.OnMessage += _onDataChannelMessage;
        }

        private IEnumerator SetRemote(RTCSessionDescription session)
        {
            yield return _peerConnection.SetRemoteDescription(ref session);
        }

        private void HandleIceCandidate(RTCIceCandidate candidate)
        {
            CandidateModel model = new CandidateModel
            {
                Candidate = candidate.Candidate,
                SdpMid = candidate.SdpMid,
                SdpMLineIndex = candidate.SdpMLineIndex,
                Foundation = candidate.Foundation,
                Priority = (int)candidate.Priority,
                Address = candidate.Address,
                Protocol = candidate.Protocol?.ToString().ToLower() ?? string.Empty,
                Port = (candidate.Port ?? 0),
                Type = candidate.Type?.ToString().ToLower() ?? string.Empty,
                TcpType = candidate.TcpType?.ToString().ToLower() ?? string.Empty,
                RelatedAddress = candidate.RelatedAddress,
                RelatedPort = (candidate.RelatedPort ?? 0),
                UsernameFragment = candidate.UserNameFragment
            };

            _client.OnCandidate(model);
        }

        private void HandleNegotiationNeeded()
        {
            _client?.OnNegotiationNeeded();
        }

        private void OnDestroy()
        {
            StopAllCoroutines();

            _dataChannel.Close();
            _peerConnection.Close();
        }

        private void HandleDataChannelMessage(byte[] bytes)
        {
            string json = Encoding.UTF8.GetString(bytes);

            DataChannelMessage message = JsonConvert.DeserializeObject<DataChannelMessage>(json);

            MessageReceived?.Invoke(message);
        }

        private RTCSdpType MapType(string type)
        {
            switch (type)
            {
                case "offer":
                    return RTCSdpType.Offer;
                case "answer":
                    return RTCSdpType.Answer;
                default:
                    return RTCSdpType.Offer;
            }
            ;
        }

        private RTCConfiguration GetSelectedSdpSemantics()
        {
            RTCConfiguration config = default;

            if (_settings != null)
            {
                config.iceServers = _settings.IceServers
                    .Select(
                        server =>
                            new RTCIceServer
                            {
                                credential = server.Credential,
                                credentialType = RTCIceCredentialType.Password,
                                urls = new string[] { server.Urls.ToString() },
                                username = server.Username,
                            }
                    )
                    .ToArray();
            }

            return config;
        }
    }
}
