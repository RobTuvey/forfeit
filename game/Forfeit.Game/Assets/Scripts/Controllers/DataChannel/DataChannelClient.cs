using Forfeit.Models;
using Unity.WebRTC;

namespace Forfeit.Controllers.DataChannel
{
    public interface IDataChannelClient
    {
        void OnCandidate(CandidateModel candidate);
        void OnConnectionStateChanged(RTCPeerConnectionState state);
        void OnIceGatheringStateChanged(RTCIceGatheringState state);
        void OnNegotiationNeeded();
        void OnOffer(OfferModel offer);
        void Awake();
        void Disable();
    }

    public abstract class DataChannelClient : IDataChannelClient
    {
        public virtual void OnCandidate(CandidateModel candidate) { }

        public virtual void OnOffer(OfferModel offer) { }

        public virtual void OnIceGatheringStateChanged(RTCIceGatheringState state) { }

        public virtual void OnConnectionStateChanged(RTCPeerConnectionState state) { }

        public virtual void OnNegotiationNeeded() { }

        public virtual void Awake() { }

        public virtual void Disable() { }
    }
}
