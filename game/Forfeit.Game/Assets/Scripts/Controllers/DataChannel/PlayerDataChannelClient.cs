using Forfeit.EventReceivers;
using Forfeit.Models;
using Forfeit.Utilities;
using Unity.WebRTC;

namespace Forfeit.Controllers.DataChannel
{
    public class PlayerDataChannelClient : DataChannelClient, IDataChannelClient
    {
        public string ConnectionId { get; private set; }

        private readonly IGameClient _gameClient;
        private readonly DataChannelController _dataChannel;

        public PlayerDataChannelClient(
            string connectionId,
            IGameClient gameClient,
            DataChannelController dataChannel
        )
        {
            ConnectionId = connectionId;

            _gameClient = gameClient;
            _dataChannel = dataChannel;
        }

        public override void Awake()
        {
            _gameClient.OnAnswer += HandleAnswer;
            _gameClient.OnCandidate += HandlePeerCandidate;
        }

        public override void Disable()
        {
            _gameClient.OnAnswer -= HandleAnswer;
            _gameClient.OnCandidate -= HandlePeerCandidate;
        }

        public override void OnOffer(OfferModel offer)
        {
            LoggerService.Info("[Network] Sending offer");
            offer.TargetConnectionId = ConnectionId;
            offer.ConnectionId = _gameClient.ConnectionId;

            _gameClient.Offer(offer);
        }

        public override void OnCandidate(CandidateModel candidate)
        {
            LoggerService.Info("[Network] Sending candidate");
            candidate.TargetConnectionId = ConnectionId;
            candidate.ConnectionId = _gameClient.ConnectionId;

            _gameClient.Candidate(candidate);
        }

        private void HandleAnswer(OfferModel offer)
        {
            LoggerService.Info("[Network] Answer received");

            if (offer.ConnectionId == ConnectionId)
            {
                _dataChannel.HandleAnswer(offer);
            }
        }

        private void HandlePeerCandidate(CandidateModel candidate)
        {
            LoggerService.Info("[Network] Candidate received");

            if (candidate.ConnectionId == ConnectionId)
            {
                _dataChannel.HandlePeerCandidate(candidate);
            }
        }

        public override void OnConnectionStateChanged(RTCPeerConnectionState state)
        {
            LoggerService.Info($"[Network] Connection state: {state}");
        }
    }
}
