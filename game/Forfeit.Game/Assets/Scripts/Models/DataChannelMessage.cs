﻿using Forfeit.Models.Events;
using System.Collections.Generic;

namespace Forfeit.Models
{
    public class DataChannelMessage
    {
        public const string BUTTON_DOWN = "ButtonDown";
        public const string BUTTON_UP = "ButtonUp";
        public const string MOVE = "Move";

        public string type { get; set; } = string.Empty;
        public object data { get; set; }
    }
}
