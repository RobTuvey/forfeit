namespace Forfeit.Models
{
    public abstract class InputModel
    {
        public abstract string Type { get; }

        public int Row { get; private set; }

        public int Column { get; private set; }

        public int Size { get; private set; }

        public string Input { get; protected set; }

        public string Label { get; protected set; }

        public string Icon { get; protected set; }

        public InputModel WithRow(int row)
        {
            Row = row;
            return this;
        }

        public InputModel WithColumn(int column)
        {
            Column = column;
            return this;
        }

        public InputModel WithSize(int size)
        {
            Size = size;
            return this;
        }
    }

    public class ButtonInputModel : InputModel
    {
        public override string Type
        {
            get => "button";
        }

        public static ButtonInputModel CreateWithLabel(string label, string input)
        {
            return new ButtonInputModel { Label = label, Input = input };
        }

        public static ButtonInputModel CreateWithIcon(string icon, string input)
        {
            return new ButtonInputModel { Icon = icon, Input = input };
        }
    }

    public class JoystickInputModel : InputModel
    {
        public override string Type
        {
            get => "joystick";
        }

        public static JoystickInputModel Create()
        {
            return new JoystickInputModel();
        }
    }
}
