﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forfeit.Models
{
    public class DataChannelSessionDescription
    {
        public string Type { get; private set; } = string.Empty;
        public string Sdp { get; private set; } = string.Empty;

        public DataChannelSessionDescription(string type, string sdp)
        {
            Type = type;
            Sdp = sdp;
        }
    }
}
