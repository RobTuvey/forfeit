﻿using System;

namespace Forfeit.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string ConnectionId { get; set; }
        public string UserName { get; set; }
    }
}
