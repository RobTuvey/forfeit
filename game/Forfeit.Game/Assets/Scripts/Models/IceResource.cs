﻿using System;
using System.Collections.Generic;

namespace Forfeit.Models
{
    public class IceResource
    {
        public string Username { get; set; } = string.Empty;

        public DateTime? UpdatedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Password { get; set; } = string.Empty;

        public List<IceServer> IceServers { get; set; } = new List<IceServer>();
    }
}
