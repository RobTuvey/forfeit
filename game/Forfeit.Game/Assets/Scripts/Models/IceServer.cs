﻿using System;

namespace Forfeit.Models
{
    public class IceServer
    {
        public string Username { get; private set; } = string.Empty;

        public string Credential { get; private set; } = string.Empty;

        public Uri Url { get; private set; }

        public Uri Urls { get; private set; }

        public IceServer(string username, string credential, Uri url, Uri urls)
        {
            Username = username;
            Credential = credential;
            Url = url;
            Urls = urls;
        }
    }
}
