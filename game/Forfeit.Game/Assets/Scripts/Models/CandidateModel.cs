﻿namespace Forfeit.Models
{
    public class CandidateModel
    {
        public string ConnectionId { get; set; } = string.Empty;

        public string TargetConnectionId { get; set; } = string.Empty;

        public string Candidate { get; set; } = string.Empty;

        public string SdpMid { get; set; } = string.Empty;

        public int? SdpMLineIndex { get; set; }

        public string Foundation { get; set; } = string.Empty;

        public int Priority { get; set; }

        public string Address { get; set; } = string.Empty;

        public string Protocol { get; set; } = string.Empty;

        public int Port { get; set; }

        public string Type { get; set; } = string.Empty;

        public string TcpType { get; set; } = string.Empty;

        public string RelatedAddress { get; set; } = string.Empty;

        public int RelatedPort { get; set; }

        public string UsernameFragment { get; set; } = string.Empty;
    }
}
