﻿namespace Forfeit.Models
{
    public class OfferModel
    {
        public string ConnectionId { get; set; } = string.Empty;
        public string TargetConnectionId { get; set; } = string.Empty;
        public DataChannelSessionDescription Description { get; set; }

        public OfferModel(string type, string sdp)
        {
            Description = new DataChannelSessionDescription(type, sdp);
        }
    }
}
