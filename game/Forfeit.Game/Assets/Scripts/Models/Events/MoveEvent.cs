﻿namespace Forfeit.Models.Events
{
    public class MoveEvent : Event
    {
        public float x { get; set; }
        public float y { get; set; }
    }
}
