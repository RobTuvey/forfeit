﻿namespace Forfeit.Models.Events
{
    public class InputEvent : Event
    {
        public string Input { get; set; } = string.Empty;
    }
}
