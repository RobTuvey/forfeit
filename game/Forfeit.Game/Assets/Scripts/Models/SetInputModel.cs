using System.Collections.Generic;
using System.Linq;

namespace Forfeit.Models
{
    public class SetInputModel
    {
        public string Group { get; private set; }

        public string ConnectionId { get; private set; }

        public List<InputModel> Inputs { get; private set; }

        public static SetInputModel SetGroupInput(string group, IEnumerable<InputModel> inputs) =>
            new SetInputModel { Group = group, Inputs = inputs.ToList() };

        public static SetInputModel SetClientInput(
            string connectionId,
            IEnumerable<InputModel> inputs
        ) => new SetInputModel { ConnectionId = connectionId, Inputs = inputs.ToList() };
    }
}
