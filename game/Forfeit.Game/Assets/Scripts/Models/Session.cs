﻿using System;

namespace Forfeit.Models
{
    public class Session
    {
        public Guid Id { get; set; }

        public string Host { get; set; } = string.Empty;

        public string Code { get; set; } = string.Empty;

        public DateTime CreatedDate { get; set; }

        public bool Completed { get; set; }

        public DateTime? CompletedDate { get; set; }
    }
}
