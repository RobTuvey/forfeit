using System;
using System.Collections;
using UnityEngine;

namespace Forfeit.Utilities
{
    public static class Coroutines
    {
        public static IEnumerator Delay(int seconds, Action action)
        {
            if (action != null && seconds > 0)
            {
                yield return new WaitForSeconds(seconds);

                action.Invoke();
            }
        }
    }
}
