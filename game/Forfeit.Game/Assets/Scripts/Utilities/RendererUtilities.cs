﻿using UnityEngine;

namespace Forfeit.Utilities
{
    public static class RendererUtilities
    {
        public static void SetModelColour(GameObject gameObject, Color color)
        {
            var renderers = gameObject.GetComponentsInChildren<Renderer>();

            foreach (var renderer in renderers)
            {
                renderer.material.color = color;
            }
        }

        public static void SetModelToRandomColour(GameObject gameObject)
        {
            var colour = GetRandomRendererColour();

            SetModelColour(gameObject, colour);
        }

        public static Color GetRandomRendererColour()
        {
            return Random.ColorHSV(0.0f, 1.0f, 1.0f, 1.0f, 0.5f, 1.0f);
        }
    }
}
