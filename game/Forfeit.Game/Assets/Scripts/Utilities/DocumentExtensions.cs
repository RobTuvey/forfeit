using System;
using UnityEngine.UIElements;

namespace Forfeit.Utilities
{
    public static class DocumentExtensions
    {
        public static void RegisterButtonClick(
            this UIDocument document,
            string elementName,
            EventCallback<ClickEvent> callback
        )
        {
            var button = document.rootVisualElement.Q<Button>(elementName);

            if (button != null)
            {
                button.RegisterCallback(callback);
            }
        }

        public static void UnregisterButtonClick(
            this UIDocument document,
            string elementName,
            EventCallback<ClickEvent> callback
        )
        {
            var button = document.rootVisualElement.Q<Button>(elementName);

            if (button != null)
            {
                button.UnregisterCallback(callback);
            }
        }

        public static void SetLabelText(this UIDocument document, string elementName, string text)
        {
            var label = document.rootVisualElement.Q<Label>(elementName);

            if (label != null)
            {
                label.text = text;
            }
        }

        public static void AppendLabel(this UIDocument document, string elementName, string text)
        {
            var label = document.rootVisualElement.Q<Label>(elementName);

            if (label != null)
            {
                label.text += $"{Environment.NewLine}{text}";
            }
        }
    }
}
