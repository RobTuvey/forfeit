﻿using System.Collections.Generic;
using System.Linq;

namespace Forfeit.Managers
{
    public interface ILocalisationManager
    {
        /// <summary>
        /// Add dictionary to the manager from the specified filepath.
        /// </summary>
        /// <param name="id">The value to identify the dictionary.</param>
        /// <param name="path">The file path.</param>
        void AddDictionaryFromPath(string id, string path);

        /// <summary>
        /// Add a dictionary to the manager.
        /// </summary>
        /// <param name="id">The value to identify the dictionary by.</param>
        /// <param name="dictionary">The dictionary.</param>
        void AddDictionary(string id, IDictionary<string, IDictionary<string, string>> dictionary);

        /// <summary>
        /// Remove a dictionary from the manager.
        /// </summary>
        /// <param name="id">The dictionary id.</param>
        void RemoveDictionary(string id);

        /// <summary>
        /// Get a localised value.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <param name="id">The value id.</param>
        /// <returns></returns>
        string Translate(string locale, string id);
    }

    public class LocalisationManager : ILocalisationManager
    {
        private List<Dictionary> _dictionaries;

        public LocalisationManager()
        {
            _dictionaries = new List<Dictionary>();
        }

        /// <summary>
        /// Add dictionary to the manager from the specified filepath.
        /// </summary>
        /// <param name="id">The value to identify the dictionary.</param>
        /// <param name="path">The file path.</param>
        public void AddDictionaryFromPath(string id, string path)
        {

        }

        /// <summary>
        /// Add a dictionary to the manager.
        /// </summary>
        /// <param name="id">The value to identify the dictionary by.</param>
        /// <param name="dictionary">The dictionary.</param>
        public void AddDictionary(string id, IDictionary<string, IDictionary<string, string>> dictionary)
        {
            _dictionaries.Add(new Dictionary(id, dictionary));
        }

        /// <summary>
        /// Remove a dictionary from the manager.
        /// </summary>
        /// <param name="id">The dictionary id.</param>
        public void RemoveDictionary(string id)
        {
            _dictionaries = _dictionaries.Where(x => x.Id != id).ToList();
        }

        /// <summary>
        /// Get a localised value.
        /// </summary>
        /// <param name="locale">The locale.</param>
        /// <param name="id">The value id.</param>
        /// <returns></returns>
        public string Translate(string locale, string id)
        {
            var dictionary = MergeDictionaries();

            if (dictionary.ContainsKey(locale))
            {
                var localeDictionary = dictionary[locale];

                if (localeDictionary.ContainsKey(id))
                {
                    return localeDictionary[id];
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Merge all of the loaded dictionaries, favouring the last loaded dictionary for conflicts.
        /// </summary>
        /// <returns></returns>
        private IDictionary<string, IDictionary<string, string>> MergeDictionaries()
            => _dictionaries
                    .Select(dictionary => dictionary.Values)
                    .SelectMany(dictionary => dictionary)
                    .GroupBy(dictionary => dictionary.Key)
                    .ToDictionary(group => group.Key, group => group.Last().Value);

        private class Dictionary
        {
            public string Id { get; set; }
            public IDictionary<string, IDictionary<string, string>> Values { get; set; }

            public Dictionary(string id, IDictionary<string, IDictionary<string, string>> values)
            {
                Id = id;
                Values = values;
            }
        }
    }
}