using Forfeit.EventReceivers;
using Forfeit.Features;
using Forfeit.Models;
using Forfeit.Utilities;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

namespace Forfeit.Managers
{
    public interface IGameManager { }

    public class GameManager : MonoBehaviour, IGameManager
    {
        private const string ROOT_GAMEOBJECT = "Root";

        private ILevelManager _levelManager;
        private IGameClient _gameClient;
        private IRootState _rootState;
        private DiContainer _container;
        private GameObject _root;

        public string StartupScene;
        public RootState RootStatePrefab;

        [Inject]
        public void Inject(
            ILevelManager levelManager,
            IGameClient gameClient,
            DiContainer container
        )
        {
            _levelManager = levelManager;
            _gameClient = gameClient;
            _container = container;
        }

        async void Start()
        {
            await Initialisation();
        }

        private async Task Initialisation()
        {
            LoggerService.Verbose("Initialising game manager.");

            _root = GameObject.Find(ROOT_GAMEOBJECT);

            if (_root != null)
            {
                _rootState = _container.Resolve<RootState>();

                _container.Bind<IRootState>().FromInstance(_rootState);

                _rootState.GameObject.transform.parent = _root.transform;
            }

            await _gameClient.InitAsync();

            LoggerService.Verbose("Game manager initialised.");
        }
    }
}
