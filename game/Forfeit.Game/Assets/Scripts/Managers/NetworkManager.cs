﻿using Forfeit.Models;
using Forfeit.Services;
using UnityEngine;
using Zenject;

namespace Forfeit.Managers
{
    public interface INetworkManager
    {
        IceResource TurnSettings { get; }
    }

    public class NetworkManager : MonoBehaviour, INetworkManager
    {
        public IceResource TurnSettings { get; private set; }

        private INetworkService _networkService;

        [Inject]
        public void Inject(INetworkService networkService)
        {
            _networkService = networkService;
        }

        private void Start()
        {
            StartCoroutine(_networkService.GetTurnSettings(settings => TurnSettings = settings));
        }
    }
}
