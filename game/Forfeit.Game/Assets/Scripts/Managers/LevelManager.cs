﻿using Forfeit.Utilities;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Forfeit.Managers
{
    public interface ILevelManager
    {
        /// <summary>
        /// Change the currently loaded scene to the scene at the path provided.
        /// </summary>
        /// <param name="path">The scene path.</param>
        void ChangeScene(string path);

        /// <summary>
        /// Load the scene at the given path.
        /// </summary>
        /// <param name="path">The scene path.</param>
        /// <returns></returns>
        IEnumerator LoadScene(string scene);
    }

    public class LevelManager : MonoBehaviour, ILevelManager
    {
        #region REGION: Constants

        public const string SCENES_PATH = "Assets/Scenes/";

        #endregion

        #region REGION: Properties

        #endregion

        #region REGION: Private Fields

        private Scene _currentScene;
        private string _currentScenePath;

        #endregion

        /// <summary>
        /// Change the currently loaded scene to the scene at the path provided.
        /// </summary>
        /// <param name="scene">The scene path.</param>
        public void ChangeScene(string scene)
        {
            if (string.IsNullOrWhiteSpace(scene))
            {
                LoggerService.Error("Invalid path; refusing to change scene.");
            }

            if (string.IsNullOrWhiteSpace(_currentScenePath) == false)
            {
                StartCoroutine(UnloadScene());
            }

            StartCoroutine(LoadScene(scene));
        }

        /// <summary>
        /// Load the scene at the given path.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public IEnumerator LoadScene(string scene)
        {
            var path = $"{SCENES_PATH}{scene}.unity";
            LoggerService.Verbose($"Loading scene at '{path}'.");

            var loadScene = SceneManager.LoadSceneAsync(path, LoadSceneMode.Additive);
            while (loadScene != null && loadScene.isDone == false)
            {
                yield return null;
            }

            _currentScene = SceneManager.GetSceneByPath(path);

            if (_currentScene.IsValid() && _currentScene.isLoaded)
            {
                _currentScenePath = path;

                LoggerService.Info($"Successfully loaded scene: '{_currentScene.name}'.");
            }
            else
            {
                LoggerService.Error($"Failed to load scene: '{path}'.");
            }
        }

        /// <summary>
        /// Unload the current scene.
        /// </summary>
        /// <returns></returns>
        private IEnumerator UnloadScene()
        {
            LoggerService.Info($"Unloading scene: '{_currentScene.name}'.");

            AsyncOperation unloadScene = SceneManager.UnloadSceneAsync(_currentScene);
            while (unloadScene.isDone == false)
            {
                yield return null;
            }

            LoggerService.Info($"Successfully unloaded scene: '{_currentScene.name}'.");

            _currentScene = default(Scene);
            _currentScenePath = string.Empty;
        }
    }
}
