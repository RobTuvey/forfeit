﻿using Forfeit.Models;
using System;
using System.Collections;

namespace Forfeit.Services
{
    public interface INetworkService
    {
        IEnumerator GetTurnSettings(Action<IceResource> callback);
    }

    public class NetworkService : INetworkService
    {
        private readonly IHttpService _httpService;

        public NetworkService(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public IEnumerator GetTurnSettings(Action<IceResource> callback)
        {
            return _httpService.Get("v1/network/turn", callback, null);
        }
    }
}
