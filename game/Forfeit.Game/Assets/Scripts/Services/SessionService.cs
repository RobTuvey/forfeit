﻿using Forfeit.EventReceivers;
using Forfeit.Models;
using System;
using System.Collections;

namespace Forfeit.Services
{
    public interface ISessionService
    {
        IEnumerator CreateSession(Action<Session> callback = null);
    }

    public class SessionService : ISessionService
    {
        private readonly IGameClient _gameClient;
        private readonly IHttpService _httpService;

        public SessionService(IGameClient gameClient, IHttpService httpService)
        {
            _gameClient = gameClient;
            _httpService = httpService;
        }

        public IEnumerator CreateSession(Action<Session> callback = null)
        {
            return _httpService.Post(
                $"v1/session",
                new { Host = _gameClient.ConnectionId },
                callback,
                null
            );
        }
    }
}
