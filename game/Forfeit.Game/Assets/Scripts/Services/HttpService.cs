﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Text;
using UnityEngine.Networking;

namespace Forfeit.Services
{
    public interface IHttpService
    {
        IEnumerator Get<TResponse>(string url, Action<TResponse> success, Action failure);
        IEnumerator Post<TData, TResponse>(
            string url,
            TData data,
            Action<TResponse> success,
            Action failure
        );
        IEnumerator Put<TData, TResponse>(
            string url,
            TData data,
            Action<TResponse> success,
            Action failure
        );
    }

    public class HttpService : IHttpService
    {
        public const string BASE_URL = "https://localhost:7087";

        public IEnumerator Get<TResponse>(string url, Action<TResponse> success, Action failure)
        {
            using (UnityWebRequest request = UnityWebRequest.Get($"{BASE_URL}/{url}"))
            {
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success)
                {
                    failure?.Invoke();
                }
                else if (success != null)
                {
                    var content = request.downloadHandler.text;

                    success(JsonConvert.DeserializeObject<TResponse>(content));
                }
            }
        }

        public IEnumerator Post<TData, TResponse>(
            string url,
            TData data,
            Action<TResponse> success,
            Action failure
        )
        {
            using (
                UnityWebRequest request = UnityWebRequest.PostWwwForm($"{BASE_URL}/{url}", string.Empty)
            )
            {
                byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));
                request.uploadHandler = new UploadHandlerRaw(body);
                request.SetRequestHeader("Content-Type", "application/json");

                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success && failure != null)
                {
                    failure?.Invoke();
                }
                else if (success != null)
                {
                    var content = request.downloadHandler.text;

                    success(JsonConvert.DeserializeObject<TResponse>(content));
                }
            }
        }

        public IEnumerator Put<TData, TResponse>(
            string url,
            TData data,
            Action<TResponse> success,
            Action failure
        )
        {
            using (
                UnityWebRequest request = UnityWebRequest.Put(
                    $"{BASE_URL}/{url}",
                    JsonConvert.SerializeObject(data)
                )
            )
            {
                yield return request.SendWebRequest();

                if (request.result != UnityWebRequest.Result.Success && failure != null)
                {
                    failure?.Invoke();
                }
                else if (success != null)
                {
                    var content = request.downloadHandler.text;

                    success(JsonConvert.DeserializeObject<TResponse>(content));
                }
            }
        }

        private IEnumerator SendRequest<TResponse>(
            UnityWebRequest request,
            Action<TResponse> success,
            Action failure
        )
        {
            yield return request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success && failure != null)
            {
                failure();
            }
            else if (success != null)
            {
                var content = request.downloadHandler.text;

                success(JsonConvert.DeserializeObject<TResponse>(content));
            }
        }
    }
}
