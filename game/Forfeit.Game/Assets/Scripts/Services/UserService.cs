﻿using Forfeit.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forfeit.Services
{
    public interface IUserService
    {
        IEnumerator GetUserById(string id, Action<User> success);
    }

    public class UserService : IUserService
    {
        private readonly IHttpService _httpService;

        public UserService(IHttpService httpService)
        {
            _httpService = httpService;
        }

        public IEnumerator GetUserById(string id, Action<User> success)
        {
            return _httpService.Get<User>($"v1/user/{id}", success, null);
        }
    }
}
