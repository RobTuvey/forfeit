param (
    [Parameter()]
    [string] $site
)

cd web

yarn build

cd ..

netlify deploy -s $site -d .\web\dist --prod