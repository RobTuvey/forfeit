$OUTPUT_FOLDER = "./dist"
$OUTPUT = "$OUTPUT_FOLDER/forfeit.exe"
$LOG = "$OUTPUT_FOLDER/log.txt"

if (! (Test-Path -Path $OUTPUT_FOLDER)) {
    New-Item -Path $OUTPUT_FOLDER -ItemType Directory
}

if (! (Test-Path -Path $LOG)) {
    New-Item -Path $LOG
}

Unity -batchmode -quit `
    -projectPath .\game\Forfeit.Game\ `
    -buildWindows64Player .\..\..\$OUTPUT `
    -logFile $LOG

Get-Content $LOG -Wait -Tail 30