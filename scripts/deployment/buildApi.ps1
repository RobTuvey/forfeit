cd server

docker build -t forfeit -f Forfeit.API/Dockerfile .

cd ..

docker tag forfeit robtuvey/forfeit:latest

docker push robtuvey/forfeit:latest