import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import path from 'path';
import babelMacros from 'vite-plugin-babel-macros';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        react({
            babel: {
                plugins: ['babel-plugin-macros'],
            },
        }),
        babelMacros(),
    ],
    resolve: {
        alias: {
            '@forfeit': path.resolve(__dirname, './src'),
            'simple-peer': 'simple-peer/simplepeer.min.js',
        },
    },
    css: {
        preprocessorOptions: {
            scss: {
                includePaths: ['./src/Styles'],
            },
        },
    },
});
