// React
import { useState } from 'react';

// Context
import {
    HubProvider,
    DataChannelProvider,
    GameContextProvider,
} from '@forfeit/Hooks';

// Models
import { Session, User } from '@forfeit/Models';

import './Styles/main.scss';
import { Routes } from './Routes';
import { ThemeProvider } from '@mui/system';
import { theme } from './Styles/theme';

export function App() {
    const [session, setSession] = useState<Session | null>(null);
    const [user, setUser] = useState<User | null>(null);

    const onGameJoined = (session: Session, user: User) => {
        setSession(session);
        setUser(user);
    };

    const onQuitGame = async () => {
        if (user && session) {
            setSession(null);
            setUser(null);
        }
    };

    return (
        <GameContextProvider>
            <DataChannelProvider>
                <HubProvider>
                    <ThemeProvider theme={theme}>
                        <div className="forfeit-app">
                            <Routes />
                        </div>
                    </ThemeProvider>
                </HubProvider>
            </DataChannelProvider>
        </GameContextProvider>
    );
}

export default App;
