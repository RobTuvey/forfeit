export type IceResource = {
    username: string;

    updatedDate?: string;

    createdDate?: string;

    password: string;

    iceServers: IceServer[];
};

export type IceServer = {
    username: string;

    credential: string;

    url: string;

    urls: string;
};