export interface Session {
    id: string;
    host: string;
    code: string;
    completed: boolean;
}
