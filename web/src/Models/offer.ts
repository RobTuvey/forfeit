import { SignalData } from 'simple-peer';

export interface Offer {
    connectionId: string;
    targetConnectionId: string;
    description: SignalData;
}
