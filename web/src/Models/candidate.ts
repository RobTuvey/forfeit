export type Candidate = RTCIceCandidate & {
    connectionId: string;
    targetConnectionId: string;
};
