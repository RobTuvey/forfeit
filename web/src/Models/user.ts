export interface User {
    id: string;
    connectionId: string;
    userName: string;
}
