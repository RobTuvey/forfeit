import { useEffect, useState, createContext, useContext } from 'react';
import {
    HubConnection,
    HubConnectionBuilder,
    HubConnectionState,
    LogLevel,
} from '@microsoft/signalr';

export interface IHubContext {
    connection: HubConnection | null;
    setConnection: React.Dispatch<React.SetStateAction<HubConnection | null>>;
    state: HubConnectionState;
    setState: React.Dispatch<React.SetStateAction<HubConnectionState>>;
}

const HubContext = createContext<IHubContext>({
    connection: null,
    setConnection: (connection) => {},
    state: HubConnectionState.Disconnected,
    setState: (state) => {},
});

export type InvokeEvents =
    | 'RemoveFromGroup'
    | 'JoinGroup'
    | 'Answer'
    | 'WebCandidate'
    | 'PlayerReady'
    | 'BroadcastConnectionId';
export type OnEvents = 'Offer' | 'PlayerReady' | 'CandidateToWeb' | 'SetInput';

export interface IHubProviderProps {
    children: React.ReactNode;
}

export function HubProvider({ children }: IHubProviderProps) {
    const [connection, setConnection] = useState<HubConnection | null>(null);
    const [state, setState] = useState(HubConnectionState.Disconnected);

    useEffect(() => {
        if (connection?.state) {
            setState(connection.state);
        }

        if (connection?.state === 'Disconnected' || !connection?.state) {
            startConnection();
        }
    }, [connection, connection?.state]);

    const startConnection = async () => {
        try {
            if (connection) {
                await connection.start();
                setState(connection.state);
            } else {
                connect();
            }
        } catch (error) {
            console.error(error);
        }
    };

    const connect = () => {
        if (!connection) {
            const connection = new HubConnectionBuilder()
                .withUrl(`${import.meta.env.VITE_ROOT_URL}v1/game`)
                .configureLogging(LogLevel.Information)
                .build();

            connection.onclose(() => {
                startConnection();
            });

            setConnection(connection);
        }
    };

    return (
        <HubContext.Provider
            value={{
                connection,
                setConnection,
                state,
                setState,
            }}
        >
            {children}
        </HubContext.Provider>
    );
}

export function useHub() {
    const { connection, setConnection, state } = useContext(HubContext);

    const disconnect = async () => {
        if (connection) {
            await connection.stop();

            setConnection(null);
        }
    };

    const invoke = async (key: InvokeEvents, ...args: unknown[]) => {
        if (connection?.state === 'Connected') {
            try {
                await connection.invoke(key, ...args);
            } catch (error) {
                console.error(error);
            }
        }
    };

    const on = <T,>(key: OnEvents, callback: (data: T) => void) => {
        if (connection) {
            connection.on(key, callback);
        }
    };

    const off = (key: OnEvents) => {
        if (connection) {
            connection.off(key);
        }
    };

    return {
        state,
        connectionId: connection?.connectionId,
        disconnect,
        invoke,
        on,
        off,
    };
}
