import { Session, User } from '@forfeit/Models';
import { ReactNode, createContext, useContext, useState } from 'react';

export interface IGameContext {
    user: Maybe<User>;
    session: Maybe<Session>;
    setUser: React.Dispatch<React.SetStateAction<Maybe<User> | undefined>>;
    setSession: React.Dispatch<React.SetStateAction<Maybe<Session>>>;
}

const GameContext = createContext<IGameContext>({
    user: undefined,
    setUser: () => {},
    session: undefined,
    setSession: () => {},
});

export interface IGameContextProviderProps {
    children?: ReactNode;
}

export function GameContextProvider({ children }: IGameContextProviderProps) {
    const [user, setUser] = useState<User>();
    const [session, setSession] = useState<Session>();

    return (
        <GameContext.Provider
            value={{
                user,
                setUser,
                session,
                setSession,
            }}
        >
            {children}
        </GameContext.Provider>
    );
}

export function useGameContext() {
    return useContext(GameContext);
}
