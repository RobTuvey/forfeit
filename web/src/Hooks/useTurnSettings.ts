import { useState, useEffect } from 'react';``
import { getTurnSettings } from "@forfeit/API";
import { IceResource } from '@forfeit/Models/ice';
export function useTurnSettings() {
    const [settings, setSettings] = useState<IceResource>();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!settings) {
            setLoading(true);
            getTurnSettings()
                .then(setSettings)
                .finally(() => setLoading(false));
        }
    }, [])

    return {
        loading,
        settings
    }
}