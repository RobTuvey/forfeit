import { IceResource } from '@forfeit/Models/ice';
import { HubConnectionState } from '@microsoft/signalr';
import {
    createContext,
    ReactNode,
    useContext,
    useEffect,
    useState,
    useCallback,
    useRef,
} from 'react';
import SimplePeer from 'simple-peer';
import { useHub } from './useHub';
import { Candidate, Offer } from '@forfeit/Models';

interface IDataChannelContext {
    connection?: SimplePeer.Instance;
    setConnection: React.Dispatch<
        React.SetStateAction<SimplePeer.Instance | undefined>
    >;
    connected: boolean;
    setConnected: React.Dispatch<React.SetStateAction<boolean>>;
}

const DataChannelContext = createContext<IDataChannelContext>({
    setConnection: (connection) => {},
    connected: false,
    setConnected: () => {},
});

interface IDataChannelProviderProps {
    children: ReactNode;
}

export function DataChannelProvider({ children }: IDataChannelProviderProps) {
    const [connection, setConnection] = useState<SimplePeer.Instance>();
    const [connected, setConnected] = useState(false);
    return (
        <DataChannelContext.Provider
            value={{
                connection,
                setConnection,
                connected,
                setConnected,
            }}
        >
            {children}
        </DataChannelContext.Provider>
    );
}

interface IUseDataChannelProps {
    onSignal?: (data: SimplePeer.SignalData) => void;
    onOffer?: (answer: SimplePeer.SignalData) => void;
    onError?: (error: any) => void;
    onConnected?: () => void;
    onCandidate?: (candidate: RTCIceCandidate) => void;
    active: boolean;
    settings?: IceResource;
}

export function useDataChannel({
    onSignal,
    onOffer,
    onError,
    onConnected,
    onCandidate,
    active,
    settings,
}: IUseDataChannelProps) {
    const { connection, setConnection, connected, setConnected } =
        useContext(DataChannelContext);
    const hub = useHub();
    const target = useRef('');

    useEffect(() => {
        if (hub.state === HubConnectionState.Connected) {
            hub.on('Offer', handleOffer);
            hub.on('CandidateToWeb', handleCandidate);
            return () => {
                hub.off('Offer');
                hub.off('CandidateToWeb');
            };
        }
    }, [hub]);

    const createConnection = () => {
        const connection = new SimplePeer({
            initiator: false,
            trickle: false,
            channelConfig: {
                ordered: false,
            },
            config: {
                iceServers: settings?.iceServers,
            },
        });

        connection
            .on('signal', handleSignal)
            .on('connect', handleConnect)
            .on('error', handleError);

        return connection;
    };

    const handleOffer = (offer: Offer) => {
        onOffer && onOffer(offer.description);
        target.current = offer.connectionId;

        disconnect();

        const connection = createConnection();

        setConnection(connection);

        connection.signal(offer.description);
    };

    const handleSignal = (data: SimplePeer.SignalData) => {
        if (data.type === 'candidate') {
            hub.invoke('WebCandidate', {
                connectionId: hub.connectionId,
                targetConnectionId: target.current,
                ...data.candidate,
            });
        } else if (data.type === 'answer') {
            hub.invoke('Answer', {
                connectionId: hub.connectionId,
                targetConnectionId: target.current,
                description: data,
            });
        }
        onSignal && onSignal(data);
    };

    const handleConnect = () => {
        setConnected(true);
        onConnected && onConnected();
    };

    const handleError = (error: any) => {
        console.error(error);
        onError && onError(error);
    };

    const handleCandidate = (candidate: Candidate) => {
        onCandidate && onCandidate(candidate);
        if (connection && !connected) {
            connection.signal({ type: 'candidate', candidate });
        }
    };

    const disconnect = () => {
        if (connection) {
            connection.destroy();
            setConnection(undefined);
            setConnected(false);
        }
    };

    return {
        connected,
        disconnect,
        connection,
    };
}

interface IEvent {}

interface IInputEvent extends IEvent {
    input: string;
}

interface IMoveEvent extends IEvent {
    x: number;
    y: number;
}

type SendDataProps =
    | { type: 'ButtonDown'; data: IInputEvent }
    | { type: 'ButtonUp'; data: IInputEvent }
    | { type: 'Move'; data: IMoveEvent };

export function useDataChannelInput() {
    const { connection, connected } = useContext(DataChannelContext);

    const sendData = (data: SendDataProps) => {
        if (connection && connected) {
            connection.send(JSON.stringify(data));
        }
    };

    return { sendData };
}
