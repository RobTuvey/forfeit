import { ReactNode, CSSProperties } from 'react';
import styles from './styles.module.scss';

export interface IStack {
    children?: ReactNode;
    direction?: CSSProperties['flexDirection'];
    justifyContent?: CSSProperties['justifyContent'];
    alignItems?: CSSProperties['alignItems'];
    gap?: CSSProperties['gap'];
}

export function Stack({
    children,
    direction = 'row',
    justifyContent,
    alignItems,
    gap,
}: IStack) {
    return (
        <div
            className={styles['stack']}
            style={{
                flexDirection: direction,
                justifyContent,
                alignItems,
                gap,
            }}
        >
            {children}
        </div>
    );
}
