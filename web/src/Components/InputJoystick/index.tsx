// Components
import { useDataChannelInput } from '@forfeit/Hooks';
import { Joystick, IJoystickEvent } from '..';
import { CSSProperties } from 'react';

export interface IInputJoystickProps {
    size?: number;
    style?: CSSProperties;
}

export function InputJoystick({ size, style }: IInputJoystickProps) {
    const { sendData } = useDataChannelInput();
    const onMove = (event: IJoystickEvent) => {
        const rotatedVector = rotate(event.x ?? 0, event.y ?? 0);
        sendData({ type: 'Move', data: { ...rotatedVector } });
    };

    /**
     * Rotate the given vectory coordinates 90 degrees.
     * @param x The x coordinate.
     * @param y The y coordinate.
     */
    const rotate = (x: number, y: number): { x: number; y: number } => {
        x *= -1;

        return { x: -y, y: -x };
    };
    return (
        <div style={style}>
            <Joystick
                throttle={100}
                size={size}
                start={onMove}
                move={onMove}
                stop={onMove}
            />
        </div>
    );
}
