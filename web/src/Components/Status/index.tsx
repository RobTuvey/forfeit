import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { Stack } from '../Stack';
import { SizeProp } from '@fortawesome/fontawesome-svg-core';
import { CSSProperties } from 'react';

export interface IStatusProps {
    hubConnected: boolean;
    dataChannelConnected: boolean;
    direction?: 'row' | 'column';
    size?: SizeProp;
    gap?: CSSProperties['gap'];
}

export function Status({
    hubConnected,
    dataChannelConnected,
    direction,
    size = '2x',
    gap = 16,
}: IStatusProps) {
    return (
        <Stack gap={16} direction={direction}>
            <FontAwesomeIcon
                icon={solid('wifi')}
                size={size}
                color={hubConnected ? 'green' : 'red'}
                style={{
                    transform:
                        direction === 'column' ? 'rotate(90deg)' : undefined,
                }}
            />

            <FontAwesomeIcon
                icon={solid('right-left')}
                size={size}
                color={dataChannelConnected ? 'green' : 'red'}
                style={{
                    transform:
                        direction === 'column' ? 'rotate(90deg)' : undefined,
                }}
            />
        </Stack>
    );
}
