import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { Input } from '../Controls';
import { InputButton } from '../InputButton';
import { InputJoystick } from '../InputJoystick';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

export interface IInputProps {
    input: Input;
}

const iconMap: Record<string, IconDefinition> = Object.freeze({
    'arrow-left': solid('arrow-left'),
    'arrow-right': solid('arrow-right'),
    'arrow-up': solid('arrow-up'),
    'arrow-down': solid('arrow-down'),
});

function getGridProperties(input: Input) {
    const column = input.column ?? 0;
    const row = input.row ?? 0;
    const size = input.size ?? 1;

    return {
        gridColumnStart: column,
        gridRowStart: row,
    };
}

export function InputControl({ input }: IInputProps) {
    if (input.type === 'button') {
        return (
            <InputButton
                input={input.input}
                size={input.size}
                style={getGridProperties(input)}
            >
                {input.label && <h2>{input.label}</h2>}
                {input.icon && iconMap[input.icon] && (
                    <FontAwesomeIcon icon={iconMap[input.icon]} size="3x" />
                )}
            </InputButton>
        );
    }

    return <InputJoystick style={getGridProperties(input)} size={input.size} />;
}
