import { Joystick as ReactJoystick } from "react-joystick-component";

export interface IJoystickEvent {
    x: number | null;
    y: number | null;
};

export interface IJoystickProps {
    baseColour?: string;
    stickColour?: string;
    throttle?: number;
    disabled?: boolean;
    move?: (event: IJoystickEvent) => void;
    start?: (event: IJoystickEvent) => void;
    stop?: (event: IJoystickEvent) => void;
    size?: number;
};

export function Joystick({
    baseColour,
    stickColour,
    throttle,
    disabled,
    move,
    start,
    stop,
    size
}: IJoystickProps) {

    const getMagnitude = (x: number, y: number) : number => {
        return Math.sqrt((x * x) + (y * y));
    };

    const normalize = (x: number, y: number) : { x: number, y: number } => {
        const magnitude = getMagnitude(x, y);
        if (magnitude > 0) {
            return {
                x: x / magnitude,
                y: y / magnitude
            };
        }

        return {
            x: 0,
            y: 0
        };
    };

    const withNormalized = (event: IJoystickEvent, callback: (event: IJoystickEvent) => void) => {
        callback(normalize(event.x ?? 0, event.y ?? 0));
    };

    return (
        <ReactJoystick 
            baseColor={baseColour}
            stickColor={stickColour}
            throttle={throttle}
            disabled={disabled}
            size={size || 150}
            move={(e) => move ? withNormalized(e, move) : null}
            start={(e) => start ? withNormalized(e, start) : null}
            stop={(e) => stop ? withNormalized(e, stop) : null}
        />  
    );
};