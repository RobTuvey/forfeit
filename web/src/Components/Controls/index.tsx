import { useEffect, useRef, useState } from 'react';
import { InputControl } from '../InputControl';
import styles from './styles.module.scss';

export type InputButtonDisplay =
    | { label: string; icon?: undefined }
    | { label?: undefined; icon: string };

type InputButton = { type: 'button'; input: string } & InputButtonDisplay;
type InputJoystick = { type: 'joystick' };

export type Input = {
    row?: number;
    column?: number;
    size?: number;
} & (InputButton | InputJoystick);

export interface IControlsProps {
    inputs: Input[];
}

function getInputKey(input: Input, index: number): string {
    if (input.type === 'joystick') {
        return `${input.type}-${index}`;
    }

    return `${input.type}-${input.input}`;
}

const COUNT = 24;

export function Controls({ inputs }: IControlsProps) {
    const [rect, setRect] = useState({ width: 0, height: 0 });
    const container = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (container.current) {
            setRect({
                width: container.current.clientWidth,
                height: container.current.clientHeight,
            });
        }
    }, [container.current]);

    return (
        <div
            className={styles['controls']}
            style={{
                gridTemplateColumns: `repeat(${COUNT}, ${
                    rect.width / COUNT
                }px)`,
                gridTemplateRows: `repeat(${COUNT}, ${rect.height / COUNT}px)`,
            }}
            ref={container}
        >
            {inputs.map((input, index) => (
                <InputControl
                    key={getInputKey(input, index)}
                    input={{
                        ...input,
                        size: input.size ? input.size * 16 : input.size,
                    }}
                />
            ))}
        </div>
    );
}
