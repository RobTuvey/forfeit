import { ReactNode } from 'react';
import styles from './styles.module.scss';

export interface IRouteContainerProps {
    children?: ReactNode;
    className?: string;
}

export function RouteContainer({ children, className }: IRouteContainerProps) {
    return (
        <div className={styles.container}>
            <div className={className}>{children}</div>
        </div>
    );
}
