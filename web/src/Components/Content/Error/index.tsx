import { Typography } from '@mui/material';

import styles from './styles.module.scss';

export interface IErrorProps {
    children?: string;
}

export function Error({ children }: IErrorProps) {
    return <Typography className={styles.error}>{children}</Typography>;
}
