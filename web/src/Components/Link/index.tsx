import { ReactNode } from 'react';
import styles from './styles.module.scss';
import { FormatClass } from '@forfeit/Util';

export interface ILinkProps {
    children?: ReactNode;
    target: string;
    button?: boolean;
}

export function Link({ children, target, button }: ILinkProps) {
    return (
        <a
            href={target}
            className={FormatClass(
                styles['link'],
                button && styles['link-button']
            )}
        >
            {children}
        </a>
    );
}
