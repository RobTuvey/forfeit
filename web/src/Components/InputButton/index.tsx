// React
import { useDataChannelInput } from '@forfeit/Hooks';
import { CSSProperties, HTMLProps, ReactNode } from 'react';

export interface IInputButtonProps extends HTMLProps<HTMLButtonElement> {
    input: string;
    children?: ReactNode;
    size?: number;
    style?: CSSProperties;
}

export function InputButton({
    input,
    children,
    style,
    ...props
}: IInputButtonProps) {
    const { sendData } = useDataChannelInput();

    const handleButtonDown = () => {
        sendData({ type: 'ButtonDown', data: { input } });
    };

    const handleButtonUp = () => {
        sendData({ type: 'ButtonUp', data: { input } });
    };

    return (
        <div
            onPointerDown={handleButtonDown}
            onPointerUp={handleButtonUp}
            style={{
                ...style,
                width: props.size || '50px',
                height: props.size || '50px',
                border: 'none',
                padding: 0,
                backgroundColor: '#999',
                borderRadius: '50%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                transform: 'rotate(90deg)',
            }}
        >
            {children}
        </div>
    );
}
