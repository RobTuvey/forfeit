export function FormatClass(
    ...classes: (string | false | undefined | null)[]
): string {
    return classes.filter((className) => Boolean(className)).join(' ');
}
