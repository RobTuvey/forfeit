import { get } from "../util";
import { IceResource } from "@forfeit/Models/ice";

export function getTurnSettings() {
    return get<IceResource>('v1/network/turn')
}