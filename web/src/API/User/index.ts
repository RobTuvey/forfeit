import { post } from './../util';
import { User } from '@forfeit/Models';

export interface ICreateUserModel { connectionId: string, code: string, name: string }
export function createUser(model: ICreateUserModel) {
    return post<ICreateUserModel, User>("v1/user", model);
}