import { get, post, deleteRequest } from './../util';
import { Session, User } from '@forfeit/Models';

export function getSessionByCode(code: string) {
    return get<Session>(`v1/session/code/${code}`);
}

export function joinSession({
    sessionCode,
    userName,
    userId,
    connectionId,
}: {
    sessionCode: string;
    userName: string;
    connectionId: string;
    userId?: string;
}) {
    return post<
        {
            sessionCode: string;
            userName: string;
            connectionId: string;
            userId?: string;
        },
        { user: User; session: Session }
    >('v1/session/join', { sessionCode, userName, userId, connectionId });
}

export function leaveSession({
    sessionId,
    userId,
}: {
    sessionId: string;
    userId: string;
}) {
    return deleteRequest(`v1/session/${sessionId}/user/${userId}`);
}
