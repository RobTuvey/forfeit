import axios from 'axios';

const api = axios.create({
    baseURL: import.meta.env.VITE_ROOT_URL,
});

export async function get<T>(url: string): Promise<T> {
    const response = await api.get<T>(url);

    if (response.status == 200) {
        return response.data;
    } else {
        throw response.statusText;
    }
}

export async function post<TData, TResponse>(url: string, data: TData) {
    const response = await api.post<TResponse>(url, data);

    if (response.status == 200) {
        return response.data;
    } else {
        throw response.statusText;
    }
}

export async function put<TData, TResponse>(url: string, data: TData) {
    const response = await api.put<TResponse>(url, data);

    if (response.status == 200) {
        return response.data;
    } else {
        throw response.statusText;
    }
}

export async function deleteRequest(url: string) {
    const response = await api.delete(url);

    if (response.status === 200) {
        return response.data;
    } else {
        throw response.statusText;
    }
}
