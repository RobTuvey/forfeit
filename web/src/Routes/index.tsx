import { Navigate, Route, Routes as NativeRoutes } from 'react-router-dom'
import { GameRoute } from './Game'
import { JoinRoute } from './Join'
import { TestRoutes } from './Test'

export function Routes() {
    return (
        <NativeRoutes>
            <Route path="*" element={<Navigate to="join" />} />
            <Route path="join" element={<JoinRoute />} />
            <Route path="game" element={<GameRoute />} />
            <Route path="test/*" element={<TestRoutes />} />
        </NativeRoutes>
    )
}
