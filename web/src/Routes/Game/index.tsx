import { Controls, Input, RouteContainer, Status } from '@forfeit/Components';
import styles from './styles.module.scss';
import {
    useDataChannel,
    useGameContext,
    useHub,
    useTurnSettings,
} from '@forfeit/Hooks';
import { HubConnectionState } from '@microsoft/signalr';
import { IconButton } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { leaveSession } from '@forfeit/API';

export function GameRoute() {
    const [inputs, setInputs] = useState<Input[]>([]);
    const { user, session } = useGameContext();
    const hub = useHub();
    const { settings } = useTurnSettings();
    const navigate = useNavigate();

    const dataChannel = useDataChannel({ active: true, settings });
    const handleQuit = async () => {
        await leaveSession({
            sessionId: session?.id ?? '',
            userId: user?.id ?? '',
        });
        dataChannel.disconnect();
        navigate('/', { replace: true });
    };

    useEffect(() => {
        if (dataChannel.connection) {
            hub.invoke('PlayerReady', session?.host);
        }
    }, [dataChannel.connection]);

    useEffect(() => {
        hub.on('SetInput', handleInputs);
        return () => hub.off('SetInput');
    }, []);

    const handleInputs = (inputs: Input[]) => {
        console.log('Inputs: ', inputs);
        setInputs(inputs);
    };

    return (
        <RouteContainer className={styles['game-container']}>
            <div className={styles['input-container']}>
                <Controls inputs={inputs} />
            </div>
            <div className={styles['status-container']}>
                <Status
                    hubConnected={hub.state === HubConnectionState.Connected}
                    dataChannelConnected={dataChannel.connected}
                    direction="column"
                    size="1x"
                />

                <IconButton onClick={handleQuit}>
                    <FontAwesomeIcon icon={solid('xmark')} size="2x" />
                </IconButton>
            </div>
        </RouteContainer>
    );
}
