import { Controls, RouteContainer } from '@forfeit/Components';

export function ControlsRoute() {
    return (
        <RouteContainer>
            <Controls
                inputs={[
                    {
                        type: 'joystick',
                        row: 2,
                        column: 10,
                        size: 10,
                    },
                    {
                        type: 'button',
                        input: 'jump',
                        icon: 'arrow-up',
                        size: 8,
                        row: 16,
                        column: 3,
                    },
                    {
                        type: 'button',
                        input: 'A',
                        label: 'A',
                        size: 8,
                        row: 20,
                        column: 12,
                    },
                ]}
            />
        </RouteContainer>
    );
}
