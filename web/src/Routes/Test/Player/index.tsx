import { useEffect, useState } from 'react';
import {
    RouteContainer,
    Status,
    InputJoystick,
    InputButton,
    Stack,
} from '@forfeit/Components';
import { useDataChannel, useHub, useTurnSettings } from '@forfeit/Hooks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { Button } from '@mui/material';

export function PlayerRoute() {
    const [ready, setReady] = useState(false);
    const { state } = useHub();
    const { settings } = useTurnSettings();
    const hub = useHub();

    const dataChannel = useDataChannel({
        active: ready,
        settings,
    });

    useEffect(() => {
        if (dataChannel.connection && ready) {
            hub.invoke('BroadcastConnectionId');
        }
    }, [dataChannel.connection, ready]);

    return (
        <RouteContainer>
            {ready && (
                <Stack direction="column">
                    <Status
                        hubConnected={state === 'Connected'}
                        dataChannelConnected={dataChannel.connected}
                    />
                    <Stack justifyContent="center" alignItems="center" gap={32}>
                        <InputJoystick />

                        <InputButton
                            input="jump"
                            className="jump-button"
                            size={150}
                        >
                            <FontAwesomeIcon
                                icon={solid('arrow-right')}
                                size="3x"
                            />
                        </InputButton>
                    </Stack>
                </Stack>
            )}
            {!ready && (
                <Stack justifyContent="center" alignItems="center">
                    <Button variant="contained" onClick={() => setReady(true)}>
                        Start
                    </Button>
                </Stack>
            )}
        </RouteContainer>
    );
}
