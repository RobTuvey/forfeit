import { Link, RouteContainer, Stack } from '@forfeit/Components';

export function MenuRoute() {
    return (
        <RouteContainer>
            <Stack
                justifyContent="center"
                alignItems="center"
                direction="column"
                gap={32}
            >
                <Link target="communication" button>
                    <h3>Network</h3>
                </Link>
                <Link target="player" button>
                    <h3>Player</h3>
                </Link>
                <Link target="controls" button>
                    <h3>Controls</h3>
                </Link>
            </Stack>
        </RouteContainer>
    );
}
