import styles from './styles.module.scss';
import { useDataChannel, useHub, useTurnSettings } from '@forfeit/Hooks';
import { Spinner } from '@forfeit/Components';
import { format } from 'date-fns';
import { useEffect, useState } from 'react';

export function CommunicationRoute() {
    const [ready, setReady] = useState(false);
    const { loading, settings } = useTurnSettings();
    const [feed, setFeed] = useState<string[]>([]);
    const hub = useHub();

    const handleSignal = () => {
        setFeed((current) => [...current, 'Offer sent']);
    };
    const handleError = (error: any) => {
        setFeed((current) => [...current, `Error: ${{ ...error }}`]);
    };

    const handleConnected = () => {
        setFeed((current) => [...current, 'Connected!']);
    };

    const handleCandidate = (candidate: RTCIceCandidate) => {
        setFeed((current) => [...current, `Candidate: ${candidate.candidate}`]);
    };

    const handleOffer = () => {
        setFeed((current) => [...current, 'Offer sent']);
    };

    const dataChannel = useDataChannel({
        active: ready,
        settings,
        onSignal: handleSignal,
        onError: handleError,
        onConnected: handleConnected,
        onCandidate: handleCandidate,
        onOffer: handleOffer,
    });

    useEffect(() => {
        if (dataChannel.connection && ready) {
            hub.invoke('BroadcastConnectionId');
        }
    }, [dataChannel.connection, ready]);

    if (!settings || loading) {
        return (
            <div
                className={`${styles.container} ${styles['loading-container']}`}
            >
                <Spinner />
            </div>
        );
    }

    return (
        <div className={styles.container}>
            <div className={`${styles.card}`}>
                <h4>Username</h4>
                {settings.username}
                <h4>Updated Date</h4>
                {settings.updatedDate
                    ? format(
                          new Date(settings.updatedDate),
                          'HH:mm dd-MMM-yyyy'
                      )
                    : '-'}
                <h4>Created Date</h4>
                {settings.createdDate
                    ? format(
                          new Date(settings.createdDate),
                          'HH:mm dd-MMM-yyyy'
                      )
                    : '-'}
                <h4>Servers</h4>
                {settings.iceServers.map((server) => (
                    <div key={server.urls}>{server.urls}</div>
                ))}

                <button onClick={() => setReady((current) => !current)}>
                    {ready ? 'Deactivate' : 'Activate'}
                </button>
            </div>

            <div className={`${styles.card} ${styles.feed}`}>
                {feed.map((item, index) => (
                    <div key={index}>{item}</div>
                ))}
            </div>
        </div>
    );
}
