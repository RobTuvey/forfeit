import { Route, Routes } from 'react-router-dom';
import { CommunicationRoute } from './Communication';
import { MenuRoute } from './Menu';
import { PlayerRoute } from './Player';
import { ControlsRoute } from './Controls';

export function TestRoutes() {
    return (
        <Routes>
            <Route path="/" element={<MenuRoute />} />
            <Route path="communication" element={<CommunicationRoute />} />
            <Route path="player" element={<PlayerRoute />} />
            <Route path="controls" element={<ControlsRoute />} />
        </Routes>
    );
}
