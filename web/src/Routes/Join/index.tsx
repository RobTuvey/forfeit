import { Error, RouteContainer, Status } from '@forfeit/Components';
import { Button, TextField } from '@mui/material';
import { useFormik, FormikProvider, ErrorMessage } from 'formik';
import { string, object } from 'yup';

import styles from './styles.module.scss';
import { useState } from 'react';
import { Stack } from '@mui/system';
import { useGameContext, useHub } from '@forfeit/Hooks';
import { createUser, getSessionByCode, joinSession } from '@forfeit/API';
import { useNavigate } from 'react-router-dom';

export type JoinModel = {
    name: string;
    code: string;
};

export function JoinRoute() {
    const { connectionId, state } = useHub();
    const { user: currentUser, setUser, setSession } = useGameContext();
    const navigate = useNavigate();

    const handleSubmit = async (model: JoinModel) => {
        try {
            if (connectionId) {
                const { user, session } = await joinSession({
                    sessionCode: model.code,
                    userName: model.name,
                    connectionId,
                    userId: currentUser?.id,
                });
                setUser(user);
                setSession(session);
                navigate('/game');
            }
        } catch (error) {
            console.error(error);
        }
    };

    const formik = useFormik({
        initialValues: { name: currentUser?.userName ?? '', code: '' },
        onSubmit: handleSubmit,
        validationSchema: object({
            name: string().required('Name is required'),
            code: string().required('Code is required'),
        }),
    });

    return (
        <RouteContainer className={styles['join-container']}>
            <div className={styles['status-container']}>
                <Status
                    hubConnected={state === 'Connected'}
                    dataChannelConnected={false}
                />
            </div>
            <FormikProvider value={formik}>
                <form
                    onSubmit={formik.handleSubmit}
                    className={styles['form-container']}
                >
                    <Stack spacing={2} className={styles['form-container']}>
                        <TextField
                            id="name"
                            label="Name"
                            {...formik.getFieldProps('name')}
                        />

                        <ErrorMessage name="name">
                            {(msg: string) => <Error>{msg}</Error>}
                        </ErrorMessage>

                        <TextField
                            id="code"
                            label="Code"
                            {...formik.getFieldProps('code')}
                        />

                        <ErrorMessage name="code">
                            {(msg: string) => <Error>{msg}</Error>}
                        </ErrorMessage>

                        <Button type="submit" variant="contained">
                            Submit
                        </Button>
                    </Stack>
                </form>
            </FormikProvider>
        </RouteContainer>
    );
}
