﻿namespace Forfeit.Domain;

public sealed class User
{
    public Guid Id { get; init; } = Guid.NewGuid();

    public required ICollection<Session> Sessions { get; init; }

    public required string ConnectionId { get; set; }

    public required string UserName { get; set; }

    public DateTimeOffset CreatedDate { get; set; } = DateTimeOffset.UtcNow;
}
