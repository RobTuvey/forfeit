﻿namespace Forfeit.Domain;

public sealed record Session
{
    public Guid Id { get; init; } = Guid.NewGuid();

    public required string HostConnectionId { get; set; }

    public required string Code { get; set; }

    public DateTimeOffset CreatedDate { get; init; } = DateTimeOffset.UtcNow;

    public DateTimeOffset? CompletedDate { get; set; }

    public bool Completed { get; set; }

    public ICollection<User> Users { get; init; } = new List<User>();
}
