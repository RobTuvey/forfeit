﻿using Forfeit.Domain;

namespace Forfeit.Application.Users.Models;

public sealed record UserDTO(Guid Id, string ConnectionId, string UserName)
{
    public UserDTO(User user)
        : this(user.Id, user.ConnectionId, user.UserName) { }
}
