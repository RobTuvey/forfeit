﻿using Forfeit.Domain;
using System.Linq.Expressions;

namespace Forfeit.Application.Users;

public static class UserQueries
{
    public static Expression<Func<User, bool>> UserByConnectionId(string connectionId) =>
        user => user.ConnectionId == connectionId;

    public static Expression<Func<User, bool>> UserById(Guid? userId) => user => user.Id == userId;
}
