﻿using Forfeit.Application.Interfaces;
using Forfeit.Application.Shared;
using Forfeit.Application.Users.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Forfeit.Application.Users;

public sealed record GetUserByIdQuery(Guid UserId) : IRequest<Result<UserDTO>>;

internal class GetUserByIdQueryHandler : IRequestHandler<GetUserByIdQuery, Result<UserDTO>>
{
    private readonly IApplicationContext _applicationContext;
    private readonly ILogger<GetUserByIdQueryHandler> _logger;

    public GetUserByIdQueryHandler(
        IApplicationContext applicationContext,
        ILogger<GetUserByIdQueryHandler> logger
    )
    {
        _applicationContext = applicationContext;
        _logger = logger;
    }

    public async Task<Result<UserDTO>> Handle(
        GetUserByIdQuery request,
        CancellationToken cancellationToken
    )
    {
        return await _applicationContext.Users
            .AsNoTracking()
            .FirstOrError(UserQueries.UserById(request.UserId), "User not found", cancellationToken)
            .Then(user => Result<UserDTO>.Ok(new(user)));
    }
}
