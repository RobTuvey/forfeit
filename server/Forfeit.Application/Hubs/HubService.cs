﻿using Forfeit.Domain;
using Microsoft.AspNetCore.SignalR;

namespace Forfeit.Application.Hubs;

public interface IHubService
{
    Task JoinGroup(string connectionId, string groupName);

    Task AddUserToGroup(User user, string groupName);

    Task RemoveUserFromGroup(User user, string groupName);

    Task CompleteSession(string connectionId, string groupName);
}

internal class HubService : IHubService
{
    private readonly IHubContext<ApplicationHub, IApplicationHub> _hubContext;

    public HubService(IHubContext<ApplicationHub, IApplicationHub> hubContext)
    {
        _hubContext = hubContext;
    }

    public async Task JoinGroup(string connectionId, string groupName)
    {
        await _hubContext.Groups.AddToGroupAsync(connectionId, groupName);
    }

    public async Task AddUserToGroup(User user, string groupName)
    {
        await _hubContext.Groups.AddToGroupAsync(user.ConnectionId, groupName);

        await _hubContext.Clients
            .GroupExcept(groupName, new List<string> { user.ConnectionId })
            .UserAdded(user.Id.ToString());
    }

    public async Task RemoveUserFromGroup(User user, string groupName)
    {
        await _hubContext.Groups.RemoveFromGroupAsync(user.ConnectionId, groupName);

        await _hubContext.Clients.Group(groupName).UserRemoved(user.Id.ToString());
    }

    public async Task CompleteSession(string connectionId, string groupName)
    {
        await _hubContext.Groups.RemoveFromGroupAsync(connectionId, groupName);

        await _hubContext.Clients.Group(groupName).SessionCompleted();
    }
}
