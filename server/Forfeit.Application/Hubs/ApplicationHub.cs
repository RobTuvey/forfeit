﻿using Microsoft.AspNetCore.SignalR;

namespace Forfeit.Application.Hubs;

public interface IApplicationHub : ISignalHub, IGameHub
{
    /// <summary>
    /// Notify clients of a new user added.
    /// </summary>
    /// <param name="userId">The id of the new user.</param>
    /// <returns></returns>
    Task UserAdded(string userId);

    /// <summary>
    /// Notify clients of a disconnected user.
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    Task UserRemoved(string userId);

    /// <summary>
    /// Notify clients the session has been completed.
    /// </summary>
    /// <returns></returns>
    Task SessionCompleted();
}

public partial class ApplicationHub : Hub<IApplicationHub> { }
