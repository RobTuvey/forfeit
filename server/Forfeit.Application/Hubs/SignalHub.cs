﻿using Forfeit.Application.Hubs.Models;
using Newtonsoft.Json;

namespace Forfeit.Application.Hubs;

public interface ISignalHub
{
    /// <summary>
    /// Method invoked from web to initiate WebRTC connection.
    /// </summary>
    /// <param name="description"></param>
    /// <returns></returns>
    Task Offer(OfferModel offer);

    /// <summary>
    /// Method invoked from game to answer WebRTC connection offer.
    /// </summary>
    /// <param name="description"></param>
    /// <returns></returns>
    Task Answer(string answer);

    /// <summary>
    /// Method to send a candidate to the web client.
    /// </summary>
    /// <param name="candidate"></param>
    /// <returns></returns>
    Task CandidateToWeb(CandidateModel candidate);

    /// <summary>
    /// Method to send a serialised candidate to the game client.
    /// </summary>
    /// <param name="candidate"></param>
    /// <returns></returns>
    Task CandidateToGame(string candidate);

    /// <summary>
    /// Method invoked from the game to notify the client the player is ready.
    /// </summary>
    /// <returns></returns>
    Task PlayerReady(string connectionId);

    /// <summary>
    /// Broadcast connection id to other clients.
    /// </summary>
    /// <returns></returns>
    Task BroadcastConnectionId(string connectionId);
}

public partial class ApplicationHub
{
    public async Task Offer(OfferModel model)
    {
        await Clients.Client(model.TargetConnectionId).Offer(model);
    }

    public async Task Candidate(CandidateModel candidate)
    {
        await Clients.Client(candidate.TargetConnectionId).CandidateToWeb(candidate);
    }

    public async Task WebCandidate(CandidateModel candidate)
    {
        await Clients
            .Client(candidate.TargetConnectionId)
            .CandidateToGame(JsonConvert.SerializeObject(candidate));
    }

    public async Task Answer(OfferModel answer)
    {
        await Clients.Client(answer.TargetConnectionId).Answer(JsonConvert.SerializeObject(answer));
    }

    public async Task PlayerReady(string connectionId)
    {
        await Clients.Client(connectionId).PlayerReady(Context.ConnectionId);
    }

    public async Task BroadcastConnectionId()
    {
        await Clients.Others.BroadcastConnectionId(Context.ConnectionId);
    }
}
