﻿namespace Forfeit.Application.Hubs.Models;

public sealed record OfferModel(
    string ConnectionId,
    string TargetConnectionId,
    SessionDescription Description
);
