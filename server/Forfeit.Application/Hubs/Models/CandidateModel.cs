﻿namespace Forfeit.Application.Hubs.Models;

public sealed record CandidateModel(
    string ConnectionId,
    string TargetConnectionId,
    string Candidate,
    string SdpMid,
    int? SdpMLineIndex,
    string Foundation,
    int Priority,
    string Address,
    string Protocol,
    int Port,
    string Type,
    string TcpType,
    string RelatedAddress,
    int RelatedPort,
    string UsernameFragment
);
