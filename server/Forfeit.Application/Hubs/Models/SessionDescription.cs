﻿namespace Forfeit.Application.Hubs.Models;

public sealed record SessionDescription(string Type, string Sdp);
