﻿namespace Forfeit.Application.Hubs.Models;

public sealed record SetInputModel(string Group, string ConnectionId, List<InputModel> Inputs);
