﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forfeit.Application.Hubs.Models;

public sealed record InputModel(
    string Type,
    string Input,
    string Label,
    string Icon,
    int Row,
    int Column,
    int Size
);
