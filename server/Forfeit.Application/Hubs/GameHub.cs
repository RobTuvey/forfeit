﻿using Forfeit.Application.Hubs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forfeit.Application.Hubs;

public interface IGameHub
{
    Task SetInput(List<InputModel> inputs);
}

public partial class ApplicationHub
{
    public async Task SetInput(SetInputModel model)
    {
        var client = Clients.Group(model.Group);

        if (string.IsNullOrEmpty(model.ConnectionId) == false)
        {
            client = Clients.Client(model.ConnectionId);
        }

        await client.SetInput(model.Inputs);
    }
}
