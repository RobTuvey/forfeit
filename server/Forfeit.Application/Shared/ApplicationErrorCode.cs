﻿namespace Forfeit.Application.Shared;

public enum ApplicationErrorCode
{
    Unknown = 0,
    InvalidParameter = 1,
    NotFound = 2,
    NotAllowed = 3,
    DatabaseError = 4,
    InvalidConfig = 5
}
