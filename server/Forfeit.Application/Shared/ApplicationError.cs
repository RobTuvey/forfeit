﻿namespace Forfeit.Application.Shared;

public sealed record ApplicationError(ApplicationErrorCode Code, string Message)
{
    public static ApplicationError Default =>
        new ApplicationError(ApplicationErrorCode.Unknown, string.Empty);
};
