﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Forfeit.Application.Shared;

public static class QueryableExtensions
{
    public static async Task<Result<TEntity>> FirstOrError<TEntity>(
        this IQueryable<TEntity> query,
        Expression<Func<TEntity, bool>> predicate,
        string? message = default,
        CancellationToken cancellationToken = default
    ) =>
        await query.FirstOrDefaultAsync(predicate, cancellationToken) switch
        {
            TEntity entity => entity,
            _
                => Result<TEntity>.Fail(
                    new ApplicationError(ApplicationErrorCode.NotFound, message ?? string.Empty)
                )
        };
}
