﻿using Newtonsoft.Json.Linq;

namespace Forfeit.Application.Shared;

public class Result
{
    protected ApplicationError? _error;

    private bool _isSuccess => _error is null;

    protected Result(ApplicationError? error)
    {
        _error = error;
    }

    public static implicit operator Result(ApplicationError error)
    {
        return new Result(error);
    }

    public static Result Ok()
    {
        return new Result(default);
    }

    public static Result Fail(ApplicationError? error)
    {
        return new Result(error);
    }

    public Result Then(Func<Result> next) =>
        _isSuccess switch
        {
            true => next(),
            false => this
        };

    public async Task<Result> Then(Func<Task<Result>> next) =>
        _isSuccess switch
        {
            true => await next(),
            false => this
        };

    public Result<TData> Then<TData>(Func<Result<TData>> next) =>
        _isSuccess switch
        {
            true => next(),
            false => Result<TData>.Fail(_error)
        };

    public async Task<Result<TData>> Then<TData>(Func<Task<Result<TData>>> next) =>
        _isSuccess switch
        {
            true => await next(),
            false => Result<TData>.Fail(_error)
        };

    public Result Finally(Action success, Action<ApplicationError> failure)
    {
        if (_isSuccess)
        {
            success();
        }
        else if (_error is not null)
        {
            failure(_error);
        }

        return this;
    }

    public async Task<Result> Finally(Func<Task> success, Action<ApplicationError> failure)
    {
        if (_isSuccess)
        {
            await success();
        }
        else if (_error is not null)
        {
            failure(_error);
        }

        return this;
    }

    public async Task<Result> Finally(Action success, Func<ApplicationError, Task> failure)
    {
        if (_isSuccess)
        {
            success();
        }
        else if (_error is not null)
        {
            await failure(_error);
        }

        return this;
    }

    public async Task<Result> Finally(Func<Task> success, Func<ApplicationError, Task> failure)
    {
        if (_isSuccess)
        {
            await success();
        }
        else if (_error is not null)
        {
            await failure(_error);
        }

        return this;
    }

    public TResult Finally<TResult>(
        Func<TResult> success,
        Func<ApplicationError, TResult> failure
    ) =>
        _isSuccess switch
        {
            true => success(),
            _ => failure(_error ?? ApplicationError.Default)
        };
}

public class Result<TValue> : Result
{
    private TValue? _value;

    protected Result(TValue value)
        : base(default)
    {
        _value = value;
    }

    protected Result(ApplicationError? error)
        : base(error)
    {
        _value = default;
    }

    public static implicit operator Result<TValue>(TValue value)
    {
        return Result<TValue>.Ok(value);
    }

    public static implicit operator Result<TValue>(ApplicationError error)
    {
        return Result<TValue>.Fail(error);
    }

    public static Result<TValue> Ok(TValue value)
    {
        return new Result<TValue>(value);
    }

    public static new Result<TValue> Fail(ApplicationError? error)
    {
        return new Result<TValue>(error);
    }

    public Result<TNext> Then<TNext>(Func<TValue, Result<TNext>> next) =>
        _value switch
        {
            TValue value => next(value),
            _ => new Result<TNext>(_error)
        };

    public Result Then(Func<TValue, Result> next) =>
        _value switch
        {
            TValue value => next(value),
            _ => this
        };

    public async Task<Result> Then(Func<TValue, Task<Result>> next) =>
        _value switch
        {
            TValue value => await next(value),
            _ => Result.Fail(_error)
        };

    public async Task<Result<TNext>> Then<TNext>(Func<TValue, Task<Result<TNext>>> next) =>
        _value switch
        {
            TValue value => await next(value),
            _ => new Result<TNext>(_error)
        };

    public Result<TValue> Finally(Action<TValue> success, Action<ApplicationError> failure)
    {
        if (_value is not null)
        {
            success(_value);
        }

        if (_error is not null)
        {
            failure(_error);
        }

        return this;
    }

    public async Task<Result<TValue>> Finally(
        Func<TValue, Task> success,
        Func<ApplicationError, Task> failure
    )
    {
        if (_value is not null)
        {
            await success(_value);
        }

        if (_error is not null)
        {
            await failure(_error);
        }

        return this;
    }

    public async Task<Result<TValue>> Finally(
        Func<TValue, Task> success,
        Action<ApplicationError> failure
    )
    {
        if (_value is not null)
        {
            await success(_value);
        }

        if (_error is not null)
        {
            failure(_error);
        }

        return this;
    }

    public async Task<Result<TValue>> Finally(
        Action<TValue> success,
        Func<ApplicationError, Task> failure
    )
    {
        if (_value is not null)
        {
            success(_value);
        }

        if (_error is not null)
        {
            await failure(_error);
        }

        return this;
    }

    public TResult Finally<TResult>(
        Func<TValue, TResult> success,
        Func<ApplicationError, TResult> failure
    )
    {
        if (_value is not null)
        {
            return success(_value);
        }

        return failure(_error ?? ApplicationError.Default);
    }
}

public static class ResultExtensions
{
    ///
    /// Then
    ///

    public static async Task<Result> Then(this Task<Result> task, Func<Task<Result>> next)
    {
        Result result = await task;

        return await result.Then(next);
    }

    public static async Task<Result> Then(this Task<Result> task, Func<Result> next)
    {
        Result result = await task;

        return result.Then(next);
    }

    public static async Task<Result> Then<TData>(
        this Task<Result<TData>> task,
        Func<TData, Result> next
    )
    {
        Result<TData> result = await task;

        return result.Then(next);
    }

    public static async Task<Result> Then<TData>(
        this Task<Result<TData>> task,
        Func<TData, Task<Result>> next
    )
    {
        Result<TData> result = await task;

        return await result.Then(next);
    }

    public static async Task<Result<TData>> Then<TData>(
        this Task<Result> task,
        Func<Task<Result<TData>>> next
    )
    {
        Result result = await task;

        return await result.Then(next);
    }

    public static async Task<Result<TData>> Then<TData>(
        this Task<Result> task,
        Func<Result<TData>> next
    )
    {
        Result result = await task;

        return result.Then(next);
    }

    public static async Task<Result<TNext>> Then<TData, TNext>(
        this Task<Result<TData>> task,
        Func<TData, Task<Result<TNext>>> next
    )
    {
        Result<TData> result = await task;

        return await result.Then(next);
    }

    public static async Task<Result<TNext>> Then<TData, TNext>(
        this Task<Result<TData>> task,
        Func<TData, Result<TNext>> next
    )
    {
        Result<TData> result = await task;

        return result.Then(next);
    }

    ///
    /// Finally
    ///

    public static async Task<Result> Finally(
        this Task<Result> task,
        Action success,
        Action<ApplicationError> failure
    )
    {
        Result result = await task;

        return result.Finally(success, failure);
    }

    public static async Task<Result> Finally(
        this Task<Result> task,
        Func<Task> success,
        Action<ApplicationError> failure
    )
    {
        Result result = await task;
        return await result.Finally(success, failure);
    }

    public static async Task<Result> Finally(
        this Task<Result> task,
        Action success,
        Func<ApplicationError, Task> failure
    )
    {
        Result result = await task;

        return await result.Finally(success, failure);
    }

    public static async Task<Result> Finally(
        this Task<Result> task,
        Func<Task> success,
        Func<ApplicationError, Task> failure
    )
    {
        Result result = await task;

        return await result.Finally(success, failure);
    }

    public static async Task<Result<TData>> Finally<TData>(
        this Task<Result<TData>> task,
        Func<TData, Task> success,
        Func<ApplicationError, Task> failure
    )
    {
        Result<TData> result = await task;

        return await result.Finally(success, failure);
    }

    public static async Task<Result<TData>> Finally<TData>(
        this Task<Result<TData>> task,
        Action<TData> success,
        Func<ApplicationError, Task> failure
    )
    {
        Result<TData> result = await task;

        return await result.Finally(success, failure);
    }

    public static async Task<Result<TData>> Finally<TData>(
        this Task<Result<TData>> task,
        Func<TData, Task> success,
        Action<ApplicationError> failure
    )
    {
        Result<TData> result = await task;

        return await result.Finally(success, failure);
    }
}
