﻿using Forfeit.Application.Shared;
using Forfeit.Domain;
using Microsoft.EntityFrameworkCore;

namespace Forfeit.Application.Interfaces;

public interface IApplicationContext
{
    DbSet<Session> Sessions { get; }
    DbSet<User> Users { get; }
    public Task<Result> SaveChangesAsync(CancellationToken cancellationToken = default);
}
