﻿using Autofac;
using Forfeit.Application.Hubs;
using Forfeit.Application.Network.Services;

namespace Forfeit.Application;

public class ContainerModule : Autofac.Module
{
    protected override void Load(ContainerBuilder builder)
    {
        base.Load(builder);

        builder.RegisterType<HubService>().AsImplementedInterfaces();
        builder.RegisterType<TwilioService>().AsImplementedInterfaces();
    }
}
