﻿using Forfeit.Application.Hubs;
using Forfeit.Application.Interfaces;
using Forfeit.Application.Shared;
using Forfeit.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Forfeit.Application.Sessions;

public sealed record CreateSessionCommand(string Host) : IRequest<Result<Session>>;

internal class CreateSessionCommandHandler : IRequestHandler<CreateSessionCommand, Result<Session>>
{
    private readonly IApplicationContext _applicationContext;
    private readonly ILogger<CreateSessionCommandHandler> _logger;
    private readonly IHubService _hubService;

    public CreateSessionCommandHandler(
        IApplicationContext applicationContext,
        ILogger<CreateSessionCommandHandler> logger,
        IHubService hubService
    )
    {
        _applicationContext = applicationContext;
        _logger = logger;
        _hubService = hubService;
    }

    public async Task<Result<Session>> Handle(
        CreateSessionCommand request,
        CancellationToken cancellationToken
    )
    {
        if (string.IsNullOrEmpty(request.Host))
        {
            return new ApplicationError(
                ApplicationErrorCode.InvalidParameter,
                "Host connection ID requrired."
            );
        }

        string code = await GetValidCode(cancellationToken);

        return await SaveNewSessionAsync(request.Host, code, cancellationToken)
            .Then(async session =>
            {
                await _hubService.JoinGroup(request.Host, code);

                return Result<Session>.Ok(session);
            });
    }

    /// <summary>
    /// Create and save a new session in the database.
    /// </summary>
    /// <param name="host">The host connection ID.</param>
    /// <param name="code">The code to use in the new session.</param>
    /// <returns></returns>
    private async Task<Result<Session>> SaveNewSessionAsync(
        string host,
        string code,
        CancellationToken cancellationToken
    )
    {
        var session = new Session { HostConnectionId = host, Code = code };

        await _applicationContext.Sessions.AddAsync(session, cancellationToken);

        await _applicationContext.SaveChangesAsync(cancellationToken);

        return session;
    }

    /// <summary>
    /// Get a valid session code, checking it is not already in use on an
    /// active session.
    /// </summary>
    /// <returns></returns>
    private async Task<string> GetValidCode(CancellationToken cancellationToken)
    {
        List<string> codesInUse = await _applicationContext.Sessions
            .Where(session => session.Completed == false)
            .Select(session => session.Code)
            .ToListAsync(cancellationToken);

        string code;
        bool isInUse;

        do
        {
            code = GenerateCode();
            isInUse = codesInUse.Contains(code);
        } while (isInUse == true);

        return code;
    }

    /// <summary>
    /// Generate a 5 digit alphanumeric code to use in a session.
    /// </summary>
    /// <returns></returns>
    private static string GenerateCode()
    {
        const string CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const int LENGTH = 5;
        var chars = new char[LENGTH];
        var random = new Random();

        for (int i = 0; i < LENGTH; i++)
        {
            chars[i] = CHARS[random.Next(CHARS.Length)];
        }

        return new string(chars);
    }
}
