﻿using Forfeit.Application.Hubs;
using Forfeit.Application.Interfaces;
using Forfeit.Application.Sessions.Model;
using Forfeit.Application.Shared;
using Forfeit.Application.Users;
using Forfeit.Application.Users.Models;
using Forfeit.Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Forfeit.Application.Sessions;

public sealed record JoinSessionCommand(
    string SessionCode,
    string UserName,
    string ConnectionId,
    Guid? UserId
) : IRequest<Result<JoinSessionResponse>>;

public sealed record JoinSessionResponse(UserDTO user, SessionDTO session)
{
    public JoinSessionResponse(User user, Session session)
        : this(new UserDTO(user), new SessionDTO(session)) { }
};

internal class JoinSessionCommandHandler
    : IRequestHandler<JoinSessionCommand, Result<JoinSessionResponse>>
{
    private readonly IApplicationContext _applicationContext;
    private readonly ILogger<JoinSessionCommandHandler> _logger;
    private readonly IHubService _hubService;

    public JoinSessionCommandHandler(
        IApplicationContext applicationContext,
        ILogger<JoinSessionCommandHandler> logger,
        IHubService hubService
    )
    {
        _applicationContext = applicationContext;
        _logger = logger;
        _hubService = hubService;
    }

    public async Task<Result<JoinSessionResponse>> Handle(
        JoinSessionCommand request,
        CancellationToken cancellationToken
    )
    {
        return await GetSession(request.SessionCode, cancellationToken)
            .Then(
                session =>
                    AddUserToSession(
                        session,
                        request.UserId,
                        request.UserName,
                        request.ConnectionId,
                        cancellationToken
                    )
            );
    }

    private async Task<Result<JoinSessionResponse>> AddUserToSession(
        Session session,
        Guid? userId,
        string userName,
        string connectionId,
        CancellationToken cancellationToken
    )
    {
        return await FindOrCreateUser(session, userId, userName, connectionId, cancellationToken)
            .Then(user => CommitChanges(user, cancellationToken))
            .Then(user => AddUserToGroup(user, session.Code))
            .Then(user => Result<JoinSessionResponse>.Ok(new JoinSessionResponse(user, session)));
    }

    private async Task<Result<User>> CommitChanges(User user, CancellationToken cancellationToken)
    {
        return await _applicationContext
            .SaveChangesAsync(cancellationToken)
            .Then(() => Result<User>.Ok(user));
    }

    private async Task<Result<User>> AddUserToGroup(User user, string code)
    {
        await _hubService.AddUserToGroup(user, code);

        return user;
    }

    private async Task<Result<Session>> GetSession(
        string code,
        CancellationToken cancellationToken
    ) =>
        await _applicationContext.Sessions.FirstOrError(
            SessionQueries.ActiveSessionByCode(code),
            "Session not found",
            cancellationToken
        );

    private async Task<Result<User>> FindOrCreateUser(
        Session session,
        Guid? userId,
        string userName,
        string connectionId,
        CancellationToken cancellationToken
    )
    {
        User? user = await _applicationContext.Users
            .Include(user => user.Sessions)
            .FirstOrDefaultAsync(UserQueries.UserById(userId));

        if (user is null)
        {
            return await CreateUser(session, userId, userName, connectionId, cancellationToken);
        }

        user.UserName = userName;
        user.ConnectionId = connectionId;

        if (user.Sessions.Any(userSession => userSession.Id == session.Id) == false)
        {
            user.Sessions.Add(session);
        }

        return user;
    }

    private async Task<Result<User>> CreateUser(
        Session session,
        Guid? userId,
        string userName,
        string connectionId,
        CancellationToken cancellationToken
    )
    {
        if (string.IsNullOrEmpty(userName))
        {
            return new ApplicationError(
                ApplicationErrorCode.InvalidParameter,
                "User name required"
            );
        }

        if (string.IsNullOrEmpty(connectionId))
        {
            return new ApplicationError(
                ApplicationErrorCode.InvalidParameter,
                "Connection ID required"
            );
        }

        User newUser = new User
        {
            Id = userId.HasValue ? userId.Value : Guid.NewGuid(),
            Sessions = new List<Session> { session },
            UserName = userName,
            ConnectionId = connectionId,
        };

        await _applicationContext.Users.AddAsync(newUser, cancellationToken);

        return newUser;
    }
}
