﻿using Forfeit.Application.Hubs;
using Forfeit.Application.Interfaces;
using Forfeit.Application.Shared;
using Forfeit.Application.Users;
using Forfeit.Domain;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Forfeit.Application.Sessions;

public sealed record LeaveSessionCommand(Guid SessionId, Guid UserId) : IRequest<Result>;

internal class LeaveSessionCommandHandler : IRequestHandler<LeaveSessionCommand, Result>
{
    private readonly IApplicationContext _applicationContext;
    private readonly IHubService _hubService;
    private readonly ILogger<LeaveSessionCommandHandler> _logger;

    public LeaveSessionCommandHandler(
        IApplicationContext applicationContext,
        IHubService hubService,
        ILogger<LeaveSessionCommandHandler> logger
    )
    {
        _applicationContext = applicationContext;
        _hubService = hubService;
        _logger = logger;
    }

    public async Task<Result> Handle(
        LeaveSessionCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _applicationContext.Sessions
            .FirstOrError(
                SessionQueries.SessionById(request.SessionId),
                "Session not found.",
                cancellationToken
            )
            .Then(session => RemoveUserFromSession(session, request.UserId, cancellationToken));
    }

    private async Task<Result> RemoveUserFromSession(
        Session session,
        Guid userId,
        CancellationToken cancellationToken
    )
    {
        return await _applicationContext.Users
            .FirstOrError(UserQueries.UserById(userId), "User not found.", cancellationToken)
            .Then(user => CommitChanges(session, user, cancellationToken))
            .Then(async user =>
            {
                await _hubService.RemoveUserFromGroup(user, session.Code);

                return Result.Ok();
            });
    }

    private async Task<Result<User>> CommitChanges(
        Session session,
        User user,
        CancellationToken cancellationToken
    )
    {
        session.Users.Remove(user);

        return await _applicationContext
            .SaveChangesAsync(cancellationToken)
            .Then(() => Result<User>.Ok(user));
    }
}
