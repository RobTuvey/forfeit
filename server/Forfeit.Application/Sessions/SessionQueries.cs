﻿using Forfeit.Domain;
using System.Linq.Expressions;

namespace Forfeit.Application.Sessions;

internal static class SessionQueries
{
    public static Expression<Func<Session, bool>> ActiveSessionByCode(string code) =>
        session => session.Completed == false && session.Code == code;

    public static Expression<Func<Session, bool>> SessionById(Guid sessionId) =>
        session => session.Id == sessionId;
}
