﻿using Forfeit.Domain;

namespace Forfeit.Application.Sessions.Model;

public sealed record SessionDTO(Guid Id, string Code, string Host, bool Completed)
{
    public SessionDTO(Session session)
        : this(session.Id, session.Code, session.HostConnectionId, session.Completed) { }
}
