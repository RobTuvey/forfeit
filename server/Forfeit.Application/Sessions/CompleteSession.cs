﻿using Forfeit.Application.Hubs;
using Forfeit.Application.Interfaces;
using Forfeit.Application.Shared;
using Forfeit.Domain;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Forfeit.Application.Sessions;

public sealed record CompleteSessionCommand(Guid SessionId) : IRequest<Result>;

internal class CompleteSessionCommandHandler : IRequestHandler<CompleteSessionCommand, Result>
{
    private readonly IApplicationContext _applcationContext;
    private readonly IHubService _hubService;
    private readonly ILogger<CompleteSessionCommandHandler> _logger;

    public CompleteSessionCommandHandler(
        IApplicationContext applicationContext,
        IHubService hubService,
        ILogger<CompleteSessionCommandHandler> logger
    )
    {
        _applcationContext = applicationContext;
        _hubService = hubService;
        _logger = logger;
    }

    public async Task<Result> Handle(
        CompleteSessionCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _applcationContext.Sessions
            .FirstOrError(
                SessionQueries.SessionById(request.SessionId),
                "Session not found",
                cancellationToken
            )
            .Then(session => CompleteSession(session, cancellationToken));
    }

    private async Task<Result> CompleteSession(Session session, CancellationToken cancellationToken)
    {
        session.Completed = true;
        session.CompletedDate = DateTimeOffset.UtcNow;

        return await _applcationContext.SaveChangesAsync(cancellationToken);
    }
}
