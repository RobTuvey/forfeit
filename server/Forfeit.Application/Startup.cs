﻿using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Forfeit.Application;

public static class Startup
{
    public static IServiceCollection ConfigureApplication(this IServiceCollection services)
    {
        services.AddMediatR(mediator =>
        {
            mediator.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
        });

        return services;
    }
}
