﻿using Forfeit.Application.Network.Interfaces;
using Forfeit.Application.Network.Models;
using Forfeit.Application.Shared;
using Microsoft.Extensions.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace Forfeit.Application.Network.Services;

internal class TwilioService : ITurnService
{
    private readonly IConfiguration _configuration;

    private string _accountSid
    {
        get => _configuration["Twilio:AccountSid"];
    }

    private string _authToken
    {
        get => _configuration["Twilio:AuthToken"];
    }

    public TwilioService(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task<Result<IceResource>> GetIceResource()
    {
        if (string.IsNullOrEmpty(_accountSid) || string.IsNullOrEmpty(_authToken))
        {
            return new ApplicationError(
                ApplicationErrorCode.InvalidConfig,
                "Missing Twilio config."
            );
        }

        TwilioClient.Init(_accountSid, _authToken);

        TokenResource resource = await TokenResource.CreateAsync();

        return MapTokenResource(resource);
    }

    private IceResource MapTokenResource(TokenResource resource) =>
        new IceResource(
            resource.Username,
            resource.DateUpdated,
            resource.DateCreated,
            resource.Password,
            resource.IceServers
                .Select(
                    server =>
                        new IceServer(server.Username, server.Credential, server.Url, server.Urls)
                )
                .ToList()
        );
}
