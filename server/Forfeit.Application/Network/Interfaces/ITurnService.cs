﻿using Forfeit.Application.Network.Models;
using Forfeit.Application.Shared;

namespace Forfeit.Application.Network.Interfaces;

internal interface ITurnService
{
    Task<Result<IceResource>> GetIceResource();
}
