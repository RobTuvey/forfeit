﻿namespace Forfeit.Application.Network.Models;

public sealed record IceResource(
    string Username,
    DateTime? UpdatedDate,
    DateTime? CreatedDate,
    string Password,
    List<IceServer> IceServers
);
