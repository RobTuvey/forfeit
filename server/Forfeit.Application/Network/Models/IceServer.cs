﻿namespace Forfeit.Application.Network.Models;

public sealed record IceServer(string Username, string Credential, Uri Url, Uri Urls);
