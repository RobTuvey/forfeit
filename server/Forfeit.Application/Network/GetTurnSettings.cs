﻿using Forfeit.Application.Network.Interfaces;
using Forfeit.Application.Network.Models;
using Forfeit.Application.Shared;
using MediatR;

namespace Forfeit.Application.Network;

public sealed record GetTurnSettingsQuery() : IRequest<Result<IceResource>>;

internal class GetTurnSettingsQueryHandler
    : IRequestHandler<GetTurnSettingsQuery, Result<IceResource>>
{
    private readonly ITurnService _turnService;

    public GetTurnSettingsQueryHandler(ITurnService turnService)
    {
        _turnService = turnService;
    }

    public async Task<Result<IceResource>> Handle(
        GetTurnSettingsQuery request,
        CancellationToken cancellationToken
    )
    {
        return await _turnService.GetIceResource();
    }
}
