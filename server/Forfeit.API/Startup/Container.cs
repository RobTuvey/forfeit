﻿using Autofac;
using Autofac.Extensions.DependencyInjection;

namespace Forfeit.API.Startup;

public static class Container
{
    public static IHostBuilder ConfigureContainer(this IHostBuilder host)
    {
        host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
        host.ConfigureContainer<ContainerBuilder>(
            container =>
                container
                    .RegisterModule(new Forfeit.Infrastructure.ContainerModule())
                    .RegisterModule(new Forfeit.Application.ContainerModule())
        );

        return host;
    }
}
