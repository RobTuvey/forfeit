﻿using Forfeit.Infrastructure;

namespace Forfeit.API.Startup;

internal static class Infrastructure
{
    public static IServiceCollection ConfigureInfrastructure(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        InfrastructureConfiguration infrastructureConfiguration = GetConfiguration(configuration);

        services.ConfigureDatabase(infrastructureConfiguration);

        return services;
    }

    private static InfrastructureConfiguration GetConfiguration(IConfiguration configuration)
    {
        InfrastructureConfiguration infrastructureConfiguration = new();

        configuration.GetSection("Infrastructure").Bind(infrastructureConfiguration);

        return infrastructureConfiguration;
    }
}
