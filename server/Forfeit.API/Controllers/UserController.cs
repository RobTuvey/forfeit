﻿using Forfeit.Application.Shared;
using Forfeit.Application.Users;
using Forfeit.Application.Users.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Forfeit.API.Controllers;

[ApiController]
[Route("v1/[controller]")]
public class UserController : BaseController
{
    private readonly ISender _sender;

    public UserController(ISender sender)
    {
        _sender = sender;
    }

    [HttpGet("{userId:guid}")]
    [ProducesResponseType(typeof(UserDTO), StatusCodes.Status200OK)]
    [EndpointName(nameof(GetUserById))]
    public async Task<IActionResult> GetUserById(
        [FromRoute] Guid userId,
        CancellationToken cancellationToken
    )
    {
        Result<UserDTO> result = await _sender.Send(
            new GetUserByIdQuery(userId),
            cancellationToken
        );

        return result.Finally(Ok, HandleError);
    }
}
