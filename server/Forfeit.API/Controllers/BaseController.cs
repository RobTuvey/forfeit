﻿using Forfeit.Application.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace Forfeit.API.Controllers;

public class BaseController : ControllerBase
{
    protected IActionResult HandleError(ApplicationError error)
    {
        ProblemDetails details = MapError(error);

        return StatusCode(details.Status ?? (int)HttpStatusCode.InternalServerError, details);
    }

    private ProblemDetails MapError(ApplicationError error) =>
        new ProblemDetails
        {
            Title = MapTitle(error.Code),
            Type = error.Code.ToString(),
            Detail = error.Message,
            Status = MapStatus(error.Code)
        };

    private string MapTitle(ApplicationErrorCode errorCode) =>
        errorCode switch
        {
            ApplicationErrorCode.InvalidParameter => "Invalid parameter",
            ApplicationErrorCode.NotFound => "Not found",
            ApplicationErrorCode.NotAllowed => "Not allowed",
            ApplicationErrorCode.InvalidConfig => "Config invalid",
            _ => "Unknown error"
        };

    private int MapStatus(ApplicationErrorCode errorCode) =>
        errorCode switch
        {
            ApplicationErrorCode.InvalidParameter => (int)HttpStatusCode.BadRequest,
            ApplicationErrorCode.NotFound => (int)HttpStatusCode.NotFound,
            ApplicationErrorCode.NotAllowed => (int)HttpStatusCode.Forbidden,
            _ => (int)HttpStatusCode.InternalServerError
        };
}
