﻿using Forfeit.Application.Sessions;
using Forfeit.Application.Shared;
using Forfeit.Domain;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Forfeit.API.Controllers;

[ApiController]
[Route("v1/[controller]")]
public class SessionController : BaseController
{
    private readonly ISender _sender;

    public SessionController(ISender sender)
    {
        _sender = sender;
    }

    [HttpPost("join")]
    [ProducesResponseType(typeof(JoinSessionResponse), StatusCodes.Status200OK)]
    [EndpointName(nameof(JoinSession))]
    public async Task<IActionResult> JoinSession(
        [FromBody] JoinSessionCommand command,
        CancellationToken cancellationToken
    )
    {
        Result<JoinSessionResponse> result = await _sender.Send(command, cancellationToken);

        return result.Finally(id => Ok(id), error => HandleError(error));
    }

    [HttpPost]
    [ProducesResponseType(typeof(Session), StatusCodes.Status200OK)]
    [EndpointName(nameof(CreateSession))]
    public async Task<IActionResult> CreateSession(
        [FromBody] CreateSessionCommand command,
        CancellationToken cancellationToken
    )
    {
        Result<Session> result = await _sender.Send(command, cancellationToken);

        return result.Finally(Ok, HandleError);
    }

    [HttpDelete("{sessionId:guid}/user/{userId:guid}")]
    [EndpointName(nameof(LeaveSession))]
    public async Task<IActionResult> LeaveSession(
        [FromRoute] Guid sessionId,
        [FromRoute] Guid userId,
        CancellationToken cancellationToken
    )
    {
        Result result = await _sender.Send(
            new LeaveSessionCommand(sessionId, userId),
            cancellationToken
        );

        return result.Finally(() => Ok(), error => HandleError(error));
    }

    [HttpPut("{sessionId:guid}/complete")]
    public async Task<IActionResult> CompleteSession(
        [FromRoute] Guid sessionId,
        CancellationToken cancellationToken
    )
    {
        Result result = await _sender.Send(
            new CompleteSessionCommand(sessionId),
            cancellationToken
        );

        return result.Finally(() => Ok(), error => HandleError(error));
    }
}
