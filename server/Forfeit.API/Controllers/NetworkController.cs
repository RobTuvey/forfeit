﻿using Forfeit.Application.Network;
using Forfeit.Application.Network.Models;
using Forfeit.Application.Shared;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Forfeit.API.Controllers;

[ApiController]
[Route("v1/[controller]")]
public class NetworkController : BaseController
{
    private readonly ISender _sender;

    public NetworkController(ISender sender)
    {
        _sender = sender;
    }

    [HttpGet("turn")]
    [ProducesResponseType(typeof(IceResource), StatusCodes.Status200OK)]
    [EndpointName(nameof(GetIceResource))]
    public async Task<IActionResult> GetIceResource(CancellationToken cancellationToken)
    {
        Result<IceResource> result = await _sender.Send(
            new GetTurnSettingsQuery(),
            cancellationToken
        );

        return result.Finally(Ok, HandleError);
    }
}
