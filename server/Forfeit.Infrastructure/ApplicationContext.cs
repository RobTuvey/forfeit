﻿using Forfeit.Application.Interfaces;
using Forfeit.Application.Shared;
using Forfeit.Domain;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Forfeit.Infrastructure;

internal class ApplicationContext : DbContext, IApplicationContext
{
    public DbSet<Session> Sessions => Set<Session>();
    public DbSet<User> Users => Set<User>();

    public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
    }

    public new async Task<Result> SaveChangesAsync(CancellationToken cancellationToken)
    {
        try
        {
            await base.SaveChangesAsync(cancellationToken);

            return Result.Ok();
        }
        catch
        {
            return Result.Fail(
                new ApplicationError(
                    ApplicationErrorCode.DatabaseError,
                    "A database error has occurred"
                )
            );
        }
    }
}
