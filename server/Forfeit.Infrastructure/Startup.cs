﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Forfeit.Infrastructure;

public static class Startup
{
    public static IServiceCollection ConfigureDatabase(
        this IServiceCollection services,
        InfrastructureConfiguration configuration
    )
    {
        services.AddDbContext<ApplicationContext>(options =>
        {
            options.UseNpgsql(configuration.DbConnectionString);
        });

        return services;
    }
}
