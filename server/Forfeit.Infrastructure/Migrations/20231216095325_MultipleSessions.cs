﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Forfeit.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class MultipleSessions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Sessions_SesssionId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_SesssionId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "SesssionId",
                table: "Users");

            migrationBuilder.CreateTable(
                name: "SessionUser",
                columns: table => new
                {
                    SessionsId = table.Column<Guid>(type: "uuid", nullable: false),
                    UsersId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionUser", x => new { x.SessionsId, x.UsersId });
                    table.ForeignKey(
                        name: "FK_SessionUser_Sessions_SessionsId",
                        column: x => x.SessionsId,
                        principalTable: "Sessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SessionUser_Users_UsersId",
                        column: x => x.UsersId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SessionUser_UsersId",
                table: "SessionUser",
                column: "UsersId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SessionUser");

            migrationBuilder.AddColumn<Guid>(
                name: "SesssionId",
                table: "Users",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Users_SesssionId",
                table: "Users",
                column: "SesssionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Sessions_SesssionId",
                table: "Users",
                column: "SesssionId",
                principalTable: "Sessions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
