﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Forfeit.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class SessionCodeIndex : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Sessions_Completed_Code",
                table: "Sessions",
                columns: new[] { "Completed", "Code" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Sessions_Completed_Code",
                table: "Sessions");
        }
    }
}
