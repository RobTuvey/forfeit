﻿using Forfeit.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Forfeit.Infrastructure.EntityConfigurations;

internal class SessionConfiguration : IEntityTypeConfiguration<Session>
{
    public void Configure(EntityTypeBuilder<Session> builder)
    {
        builder.HasKey(session => session.Id);

        builder.HasMany(session => session.Users).WithMany(user => user.Sessions);

        builder.HasIndex(session => new { session.Completed, session.Code });
    }
}
