﻿using Autofac;

namespace Forfeit.Infrastructure;

public class ContainerModule : Autofac.Module
{
    protected override void Load(ContainerBuilder builder)
    {
        base.Load(builder);

        builder
            .RegisterType<ApplicationContext>()
            .AsImplementedInterfaces()
            .InstancePerLifetimeScope();
    }
}
